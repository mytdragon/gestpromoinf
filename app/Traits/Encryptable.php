<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 08.02.2019
 */

namespace App\Traits;

use Illuminate\Support\Facades\Crypt;

/**
 * Trait that encrypt/decrypt fields in model on actions creating, updating and retrieved.
 * It uses the array "$encryptable" in the model.
 */
trait Encryptable
{
    /**
     * Boot function from laravel.
     */
    protected static function bootEncryptable()
    {
        /**
         * When model is creating, encrypt values which need to be encrypter
         */
        static::creating(function ($model) {
          if ($model->encryptable) {
            foreach ($model->encryptable as $key){
              if ($model->{$key}){
                $model->{$key} = Crypt::encryptString($model->{$key});
              }
            }
          }
        });

        /**
         * When model is updating, encrypt values which need to be encrypter
         */
        static::updating(function ($model) {
          if ($model->encryptable) {
            foreach ($model->encryptable as $key){
              if ($model->{$key}){
                $model->{$key} = Crypt::encryptString($model->{$key});
              }
            }
          }
        });

        /**
         * When model is retrieved, decrypt values which need to be encrypter in the collection returned
         */
        static::retrieved(function ($model) {
          if ($model->encryptable) {
            foreach ($model->encryptable as $key){
              if ($model->{$key}){
                $model->{$key} = Crypt::decryptString($model->{$key});
              }
            }
          }
        });
    }
}
