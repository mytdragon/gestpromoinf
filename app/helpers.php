<?php

/**
 * Round a number to nearest fifth
 * @param  float|integer $value value to round
 * @return float|integer        rounded value
 */
function roundFifth($value){
  if (!$value){
    return $value;
  }

  $whole = floor($value);
  $decimals = $value - $whole;
  if ($whole == $value){
    return $value;
  }
  else {
    switch (true){
      case $decimals < 0.25:
        return $whole;
        break;
      case $decimals >= 0.75:
        return $whole + 1;
        break;
      default:
        return $whole + 0.5;
        break;
    }
  }
}

/**
 * Round a number to nearest tenth
 * @param  float|integer $value value to round
 * @return float|integer        rounded value
 */
function roundTenth($value){
  if (!$value) { return null; }
  return round($value, 1);
}
