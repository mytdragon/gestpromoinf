<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 13.02.2019
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Validator;
use Lang;
use App\Models\Student;
use App\Models\Class_;
use App\Models\Promotion;
use App\Models\Lesson;
use App\Models\Grade;

/**
 * Controller for Student model
 */
class StudentController extends Controller
{
    /**
     * Show list of student (can also accept search params)
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function showStudents(Request $request){
      if($request->search) {
         $students = Student::all()->filter(function ($student) use ($request){
           $search = strtolower($request->search);
           switch ($request->search) {
              case 'archivé':
                return $student->stuArchived;
                break;
              case 'désarchivé':
                return !$student->stuArchived;
                break;
              default:
                $fields =
                  $student->idStudent
                  .';'.$student->stuFirstname
                  .' '.$student->stuLastname
                  .';'.$student->class->claName
                  .';'.$student->class->formation->forName
                  .';'.$student->class->teacher->teaFirstname
                  .' '.$student->class->teacher->teaLasstname;
                $fields = strtolower($fields);
                return strpos($fields, $search) !== false;
           }
         });
       }
       else {
        $students = Student::all();
       }

      return view('student.students', ['students' => $students]);
    }

    /**
     * Show student's grades with average, promotion, etc
     * @param  string|integer $id student's id
     * @return Illuminate\View\view
     */
    public function view($id){
      $student = Student::findOrFail($id);

      return view('student.details', [
        'student' => $student,
        'promotions' => Promotion::all(),
        'gradesFormated'=> $student->followUp(),
        'years' => $student->class->formation->forYears
      ]);
    }


    /**
     * Show add form
     * @return \Illuminate\View\View
     */
    public function showAdd(){
      return view('student.form', [
        'classes' => Class_::all(),
        'action' => 'add'
      ]);
    }

    /**
     * Show edit form
     * @param  integer|string $id student's id
     * @return \Illuminate\View\View
     */
    public function showEdit($id){
      $student = Student::findOrFail($id);

      return view('student.form', [
        'classes' => Class_::all(),
        'action' => 'edit',
        'student' => $student
      ]);
    }

    /**
     * Add a student
     * @param Illuminate\Http\Request $request form params
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request){
      $validator = $this->validate($request, [
        'firstname' => 'required|max:50',
        'lastname' => 'required|max:50',
        'class' => 'required|not_in:0'
      ]);

      // Check if all data exists
      $class = Class_::findOrFail($request->class);

      $student = new Student();
      $student->stuFirstname = $request->firstname;
      $student->stuLastname = $request->lastname;
      $student->class()->associate($class);
      $student->save();
      $student->refresh();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.addSucces.title'), 'msg' => Lang::get('gest.addSucces.msg', array('model' => $student->stuFirstname.' '.$student->stuLastname))]);
      return redirect()->route('students');
    }

    /**
     * Edit a student
     * @param  Illuminate\Http\Request $request form paramas
     * @param  string|integer  $id      student's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id){
      $validator = $this->validate($request, [
        'firstname' => 'required|max:50',
        'lastname' => 'required|max:50',
        'class' => 'required|not_in:0'
      ]);

      // Check if all data exist
      $student = Student::findOrFail($id);
      $class = Class_::findOrFail($request->class);

      $student->stuFirstname = $request->firstname;
      $student->stuLastname = $request->lastname;

      // Change of class/formation
      $oldFormation = substr(strtoupper($student->class->formation->forName), 0, 1);
      $newFormation = substr(strtoupper($class->formation->forName), 0, 1);
      if ($oldFormation == $newFormation){
        $student->class()->associate($class);// Change spe/year (or same, whatever)
      }
      else {
        if ($oldFormation == 'M' && $newFormation == 'C'){
          // Copy the ANGL of MPT to ANGT of ECG
          $angl = Lesson::where('lesName', '=', 'ANGL')->firstOrFail();
          $angt = Lesson::where('lesName', '=', 'ANGT')->firstOrFail();
          foreach ($student->grades->where('lesson', $angl) as $grade){
            $angtGrade = new Grade();
            $angtGrade->graNumber = $grade->graNumber;
            $angtGrade->graSemester = $grade->graSemester;
            $angtGrade->graRepetition = $grade->graRepetition;
            $angtGrade->student()->associate($student);
            $angtGrade->lesson()->associate($angt);
            $angtGrade->save();
          }

          // Copy the SCINAT of MPT to SNAT of ECG
          $scinat = Lesson::where('lesName', '=', 'SCINAT')->firstOrFail();
          $snat = Lesson::where('lesName', '=', 'SNAT')->firstOrFail();
          foreach ($student->grades->where('lesson', $scinat) as $grade){
            $snatGrade = new Grade();
            $snatGrade->graNumber = $grade->graNumber;
            $snatGrade->graSemester = $grade->graSemester;
            $snatGrade->graRepetition = $grade->graRepetition;
            $snatGrade->student()->associate($student);
            $snatGrade->lesson()->associate($snat);
            $snatGrade->save();
          }

          // Copy the ECDR of MPT to ECOE of ECG
          $ecdr = Lesson::where('lesName', '=', 'ECDR')->firstOrFail();
          $ecoe = Lesson::where('lesName', '=', 'ECOE')->firstOrFail();
          foreach ($student->grades->where('lesson', $ecdr) as $grade){
            $ecoeGrade = new Grade();
            $ecoeGrade->graNumber = $grade->graNumber;
            $ecoeGrade->graSemester = $grade->graSemester;
            $ecoeGrade->graRepetition = $grade->graRepetition;
            $ecoeGrade->student()->associate($student);
            $ecoeGrade->lesson()->associate($ecoe);
            $ecoeGrade->save();
          }

          // Copy the MATHSPE or MATHFON of MPT to MATHCFC of ECG
          $mathfon = Lesson::where('lesName', '=', 'MATHFON')->firstOrFail();
          $mathspe = Lesson::where('lesName', '=', 'MATHSPE')->firstOrFail();
          $mathcfc = Lesson::where('lesName', '=', 'MATHCFC')->firstOrFail();
          foreach ($student->grades->where('lesson', $mathfon) as $grade){
            // if there is also mathspe in the same semester than mathfon, take average of both
            $mathspeGrade = $student->grades->where('lesson', $mathspe)->where('graSemester', $grade->graSemester)->where('graRepetition', $grade->graRepetition);
            if ($mathspeGrade->isEmpty()){
              $gradeNumber = $grade->graNumber;
            }
            else {
              $gradeNumber = roundFifth(($mathspeGrade->graNumber + $grade->graNumber)/2);
            }

            $mathcfcGrade = new Grade();
            $mathcfcGrade->graNumber = $gradeNumber;
            $mathcfcGrade->graSemester = $grade->graSemester;
            $mathcfcGrade->graRepetition = $grade->graRepetition;
            $mathcfcGrade->student()->associate($student);
            $mathcfcGrade->lesson()->associate($mathcfc);
            $mathcfcGrade->save();
          }
          foreach ($student->grades->where('lesson', $mathspe) as $grade){
            // if there is mathfon grade in the same semester, do nothing because we did in mathfon foreach
            $mathfonGrade = $student->grades->where('lesson', $mathfon)->where('graSemester', $grade->graSemester)->where('graRepetition', $grade->graRepetition);
            if ($mathspeGrade->isEmpty()){
              $mathcfcGrade = new Grade();
              $mathcfcGrade->graNumber = $grade->graNumber;
              $mathcfcGrade->graSemester = $grade->graSemester;
              $mathcfcGrade->graRepetition = $grade->graRepetition;
              $mathcfcGrade->student()->associate($student);
              $mathcfcGrade->lesson()->associate($mathcfc);
              $mathcfcGrade->save();
            }
          }

          // Make change
          $student->class()->associate($class);
        }
        else {
          $request->session()->flash('message', ['result' => false,'title' => Lang::get('gest.cannotChangeFormation.title'), 'msg' => Lang::get('gest.cannotChangeFormation.msg')]);
          return redirect()->route('editStudent', ['id' => $id]);
        }
      }

      $student->save();
      $student->refresh();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.editSucces.title'), 'msg' => Lang::get('gest.editSucces.msg', array('model' => $student->stuFirstname.' '.$student->stuLastname))]);
      return redirect()->route('students');
    }

    /**
     * Archive a student
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      student's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function archive(Request $request, $id){
      // Check if all data exist
      $student = Student::findOrFail($id);

      $student->stuArchived = true;
      $student->save();
      $student->refresh();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.archiveSucces.title'), 'msg' => Lang::get('gest.archiveSucces.msg', array('model' => $student->stuFirstname.' '.$student->stuLastname))]);
      return redirect()->route('students');
    }

    /**
     * Unarchive a student
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      student's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unarchive(Request $request, $id){
     // Check if all data exist
     $student = Student::findOrFail($id);

     $student->stuArchived = false;
     $student->save();
     $student->refresh();

     $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.unarchiveSucces.title'), 'msg' => Lang::get('gest.unarchiveSucces.msg', array('model' => $student->stuFirstname.' '.$student->stuLastname))]);
     return redirect()->route('students');
    }

    /**
     * Add a grade via ajax request
     * @param Request $request AJAX's params
     * @return Illuminate\Http\Response JSON result
     */
    public function setPromotion(Request $request){
      $validator = Validator::make($request->all(), [
        'student' => 'required|integer',
        'promotion' => 'required|integer',
        'semester' => 'required|integer',
        'repetition' => 'required|boolean',
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors'=>$validator->errors()->all()
        ]);
      }

      $idStudent = $request->student;
      $idPromotion = $request->promotion;
      $semester = $request->semester;
      $repeat = $request->repetition;

      $student = Student::findOrFail($idStudent);
      $promotion = $idPromotion == 0 ? null : Promotion::findOrFail($idPromotion);

      // Update or remove if exist, else add
      $studentsPromotion = $student->promotions->where('pivot.sprSemester', '=', $semester)->where('pivot.sprRepetition', '=', $repeat)->first();
      if ($studentsPromotion){
        if ($promotion){
          // Remove old
          $student->promotions()->wherePivot('sprSemester', '=', $semester)->wherePivot('sprRepetition', '=', $repeat)->detach();

          // add new
          $student->promotions()->attach($promotion, ['sprSemester' => $semester, 'sprRepetition' => $repeat]);
        }
        else {
          // If no promotion, then remove
          $student->promotions()->wherePivot('sprSemester', '=', $semester)->wherePivot('sprRepetition', '=', $repeat)->detach();
        }
      }
      else {
       $student->promotions()->attach($promotion, ['sprSemester' => $semester, 'sprRepetition' => $repeat]);
      }

      return response()->json([
        'success'=> true,
      ]);
    }

    /**
     * Download the view student's follow-up in PDF
     * @param  string|integer $id student's id
     * @return Illuminate\Http\Response     PDF file download
     */
    public function downloadPDF($id){
      $student = Student::findOrFail($id);

      $pdf = SnappyPdf::loadView('student.pdf-details', [
        'student' => $student,
        'gradesFormated'=> $student->followUp()
      ]);

      return $pdf->download($student->class->claName.'-'.$student->stuFirstname.'_'.$student->stuLastname.'.pdf');
    }
}
