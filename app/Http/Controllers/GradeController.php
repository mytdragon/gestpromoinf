<?php
/**
 * Autheur: Loïc Herzig
 * Date: 15.02.2019
 * ETML
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Lang;
use App\Models\Grade;
use App\Models\Student;
use App\Models\Lesson;
use App\Models\Class_;
use App\Models\Promotion;

/**
 * Controller for model Grade
 */
class GradeController extends Controller
{
    /**
     * Add a grade via ajax request
     * @param Request $request AJAX's params
     * @return Illuminate\Http\Response JSON result
     */
    public function add(Request $request){
      $validator = Validator::make($request->all(), [
        'student' => 'required|integer',
        'lesson' => 'required',
        'semester' => 'required|integer',
        'repetition' => 'required|boolean',
        'grade' => 'required|between:1,6'
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors'=>$validator->errors()->all()
        ]);
      }

      // Check if values exist
      $student = Student::findOrFail($request->student);
      $lesson = Lesson::where('lesName', $request->lesson)->firstOrFail();

      // Add the grade
      $grade = new Grade();
      $grade->graNumber = $request->grade;
      $grade->graSemester = $request->semester;
      $grade->graRepetition = $request->repetition;
      $grade->student()->associate($student);
      $grade->lesson()->associate($lesson);
      $grade->save();

      $avgToChange = Self::updateAvgForGrade($grade);

      return response()->json([
        'success'=> true,
        'avgToChange' => $avgToChange
      ]);
    }

    /**
     * Edit a grade via ajax request
     * @param Request $request AJAX's params
     * @return Illuminate\Http\Response JSON result
     */
    public function edit(Request $request){
      $validator = Validator::make($request->all(), [
        'student' => 'required|integer',
        'lesson' => 'required',
        'semester' => 'required|integer',
        'repetition' => 'required|boolean',
        'grade' => 'required|between:1,6'
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors'=>$validator->errors()->all()
        ]);
      }

      // Check if values exist
      $student = Student::findOrFail($request->student);
      $lesson = Lesson::where('lesName', $request->lesson)->firstOrFail();
      $grade = Grade::where('fkStudent', $student->idStudent)
        ->where('fkLesson', $lesson->idLesson)
        ->where('graSemester', $request->semester)
        ->where('graRepetition', $request->repetition)
        ->firstOrFail();

      // Edit the grade
      $grade->graNumber = $request->grade;
      $grade->save();

      $avgToChange = Self::updateAvgForGrade($grade);

      return response()->json([
        'success'=> true,
        'avgToChange' => $avgToChange
      ]);
    }

    /**
     * Remove a grade via ajax request
     * @param Request $request AJAX's params
     * @return Illuminate\Http\Response JSON result
     */
    public function remove(Request $request){
      $validator = Validator::make($request->all(), [
        'student' => 'required|integer',
        'lesson' => 'required',
        'semester' => 'required|integer',
        'repetition' => 'required|boolean'
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors'=>$validator->errors()->all()
        ]);
      }

      // Check if values exist
      $student = Student::findOrFail($request->student);
      $lesson = Lesson::where('lesName', $request->lesson)->firstOrFail();
      $grade = Grade::where('fkStudent', $student->idStudent)
        ->where('fkLesson', $lesson->idLesson)
        ->where('graSemester', $request->semester)
        ->where('graRepetition', $request->repetition)
        ->firstOrFail();

      // Remove the grade
      $grade->delete();

      $avgToChange = Self::updateAvgForGrade($grade);

      return response()->json([
        'success'=> true,
        'avgToChange' => $avgToChange
      ]);
    }

    /**
     * Get changes of grades on grade change
     * @param  Grade  $grade Grade changed
     * @return array        array of changes (id html => new value)
     */
    private function updateAvgForGrade(Grade $grade){
      $changes = [];

      $followup = $grade->student->followUp();
      $semester = $grade->graRepetition ? $grade->graSemester.'\'' : $grade->graSemester;
      $semesterIndex = array_search($semester,$followup['semesters']['no']);

      // NOTE I could use "in_array" for CBE/ECG to group in one case because only the cat name change
      switch ($grade->lesson->category->catName){
        case 'Théorie MPT':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg lesson
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['Théorie MPT']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // Avg MPT semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['Théorie MPT']['avgSemesters'][$semesterIndex]) ];
          // Avg MPT global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['Théorie MPT']['avgGlobal']) ];
          // Avg MPT years
          if (isset($followup['categories']['Théorie MPT']['avgYears'])){
            foreach ($followup['categories']['Théorie MPT']['avgYears'] as $year => $number){
              $changes[] = ['mpt-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'Sciences naturelles':
          // Avg lesson
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['Sciences naturelles']['lessons'][$grade->lesson->lesName]['grades'][$semesterIndex]) ];
          break;

        case 'TIB':
          // Nothing to do
          break;

        case 'CBE':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg lesson
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['CBE']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // Avg CBE semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['CBE']['avgSemesters'][$semesterIndex]) ];
          // Avg CBE global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['CBE']['avgGlobal']) ];
          // Avg CBE years
          if (isset($followup['categories']['CBE']['avgYears'])){
            foreach ($followup['categories']['CBE']['avgYears'] as $year => $number){
              $changes[] = ['cbe-year'.$year => roundTenth($number)];
            }
          }
          // Avg global CFC semester
          $changes[] = ['globalcfc-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['globalCFC'][$semesterIndex]) ];
          // Avg global CFC years
          if (isset($followup['otherAverages']['years']['globalCFC'])){
            foreach ($followup['otherAverages']['years']['globalCFC'] as $year => $number){
              $changes[] = ['globalcfc-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'ECG':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg lesson
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['ECG']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // Avg ECG semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['ECG']['avgSemesters'][$semesterIndex]) ];
          // Avg ECG global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['ECG']['avgGlobal']) ];
          // Avg ECG years
          if (isset($followup['categories']['ECG']['avgYears'])){
            foreach ($followup['categories']['ECG']['avgYears'] as $year => $number){
              $changes[] = ['cbe-year'.$year => roundTenth($number)];
            }
          }
          // Avg global CFC semester
          $changes[] = ['globalcfc-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['globalCFC'][$semesterIndex]) ];
          // Avg global CFC years
          if (isset($followup['otherAverages']['years']['globalCFC'])) {
            foreach ($followup['otherAverages']['years']['globalCFC'] as $year => $number){
              $changes[] = ['globalcfc-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'Modules I':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg module
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['Modules I']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // Avg Module I semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['Modules I']['avgSemesters'][$semesterIndex]) ];
          // Avg Module I global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['Modules I']['avgGlobal']) ];
          // Avg Modules semester
          $changes[] = ['modules-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['modules'][$semesterIndex]) ];
          // Avg Modules global
          $changes[] = ['modules-global' => roundTenth($followup['otherAverages']['avgGlobal']['modules']) ];
          // Avg Modules years
          if (isset($followup['otherAverages']['years']['modules'])){
            foreach ($followup['otherAverages']['years']['modules'] as $year => $number){
              $changes[] = ['modules-year'.$year => roundTenth($number)];
            }
          }
          // Avg global CFC semester
          $changes[] = ['globalcfc-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['globalCFC'][$semesterIndex]) ];
          // Avg global CFC years
          if (isset($followup['otherAverages']['years']['globalCFC'])){
            foreach ($followup['otherAverages']['years']['globalCFC'] as $year => $number){
              $changes[] = ['globalcfc-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'Modules C':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg module
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['Modules C']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // Avg Module I semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['Modules C']['avgSemesters'][$semesterIndex]) ];
          // Avg Module I global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['Modules C']['avgGlobal']) ];
          // Avg Modules semester
          $changes[] = ['modules-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['modules'][$semesterIndex]) ];
          // Avg Modules global
          $changes[] = ['modules-global' => roundTenth($followup['otherAverages']['avgGlobal']['modules']) ];
          // Avg Modules years
          if (isset($followup['otherAverages']['years']['modules'])){
            foreach ($followup['otherAverages']['years']['modules'] as $year => $number){
              $changes[] = ['modules-year'.$year => roundTenth($number)];
            }
          }
          // Avg global CFC semester
          $changes[] = ['globalcfc-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['globalCFC'][$semesterIndex]) ];
          // Avg global CFC years
          if (isset($followup['otherAverages']['years']['globalCFC'])){
            foreach ($followup['otherAverages']['years']['globalCFC'] as $year => $number){
              $changes[] = ['globalcfc-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'Pratique':
          $cat = preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->category->catName)));
          // Avg prat
          $changes[] = ['lesson'.preg_replace('/\W+/','',strtolower(strip_tags($grade->lesson->lesName))).'-sem-avg' => roundTenth($followup['categories']['Pratique']['lessons'][$grade->lesson->lesName]['avgGlobal']) ];
          // avg semester
          $changes[] = ['cat'.$cat.'-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['categories']['Pratique']['avgSemesters'][$semesterIndex]) ];
          // avg global
          $changes[] = ['cat'.$cat.'-global' => roundTenth($followup['categories']['Pratique']['avgGlobal']) ];
          // avg pratique years
          if (isset($followup['categories']['Pratique']['avgYears'])){
            foreach ($followup['categories']['Pratique']['avgYears'] as $year => $number){
              $changes[] = ['pratique-year'.$year => roundTenth($number)];
            }
          }
          // Avg global CFC semester
          $changes[] = ['globalcfc-sem'.$grade->graSemester.'-r'.$grade->graRepetition => roundTenth($followup['otherAverages']['semesters']['globalCFC'][$semesterIndex]) ];
          // Avg global CFC years
          if (isset($followup['otherAverages']['years']['globalCFC'])) {
            foreach ($followup['otherAverages']['years']['globalCFC'] as $year => $number){
              $changes[] = ['globalcfc-year'.$year => roundTenth($number)];
            }
          }
          break;

        case 'TIP':
          // Nothing to do
          break;

        default:
          // nothing to do
          break;
      }

      return $changes;
    }

    public function addRepeatSemester(Request $request){
      $validator = Validator::make($request->all(), [
        'student' => 'required|integer',
        'semester' => 'required|integer'
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors'=>$validator->errors()->all()
        ]);
      }

      $student = Student::findOrFail($request->student);

      try {
        $followup = $student->followUp($request->semester);
      } catch (\Exception $e) {
        return response()->json([
          'success' => false,
          'errors'=> [$e->getMessage()]
        ]);
      }

      $view = view('student.followup', [
        'student' => $student,
        'promotions' => Promotion::all(),
        'gradesFormated' => $followup,
        'years' => $student->class->formation->forYears
      ])->render();

      return response()->json([
        'success' => true,
        'html'=> $view
      ]);
    }
}
