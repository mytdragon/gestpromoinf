<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 13.02.2019
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Lang;
use App\Models\Class_;
use App\Models\Formation;
use App\Models\Teacher;
use App\Models\Student;

/**
 * Controller for class model
 */
class ClassController extends Controller
{
    /**
     * Show list of class (can also accept search params)
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function showClasses(Request $request){
      if($request->search) {
         $classes = Class_::all()->filter(function ($class) use ($request){
           $search = strtolower($request->search);
           switch ($request->search) {
              case 'archivé':
                return $class->lesArchived;
                break;
              case 'désarchivé':
                return !$class->lesArchived;
                break;
              default:
                $fields = $class->idClass.';'.$class->claName.';'.$class->teacher->teaAcronym.';'.$class->teacher->teaFirstname.' '.$class->teacher->teaLastname;
                $fields = strtolower($fields);
                return strpos($fields, $search) !== false;
           }
         });
       }
       else {
        $classes = Class_::all();
       }

      return view('class.classes', ['classes' => $classes]);
    }

    /**
     * Show class
     * @param  string|integer $id class's id
     * @return Illuminate\View\view
     */
    public function view($id, $semester = 1){
      $class = Class_::findOrFail($id);

      // Check semester
      if ($class->formation->forYears < $semester/2 || $semester == 0){
        abort(404);
      }

      // Get lessons by category
      $lessonsByCategory = $class->formation->lessons->groupBy('category.catName')->map(function($lesson) { return $lesson->pluck('lesName', 'idLesson'); });

      // Get students's follow-up
      $students = [];
      $i = 0;
      foreach ($class->students->sortBy('stuFirstname') as $student){
        $students[$i]['id'] = $student->idStudent;
        $students[$i]['name'] = $student->stuFirstname.' '.$student->stuLastname;
        $students[$i]['followup'] = $student->followUp();
        $i++;
      }

      return view('class.details', [
        'class' => $class,
        'students' => $students,
        'semester' => $semester,
        'lessonsByCategory' => $lessonsByCategory
      ]);
    }

    /**
     * Show add form
     * @return \Illuminate\View\View
     */
    public function showAdd(){
      return view('class.form', [
        'formations' => Formation::all(),
        'teachers' => Teacher::all()->reject(function($teacher) {return $teacher->teaFirstname == 'admin';}),
        'action' => 'add'
      ]);
    }

    /**
     * Show edit form
     * @param  integer|string $id class's id
     * @return \Illuminate\View\View
     */
    public function showEdit($id){
      $class = Class_::findOrFail($id);

      return view('class.form', [
        'formations' => Formation::all(),
        'teachers' => Teacher::all()->reject(function($teacher) {return $teacher->teaFirstname == 'admin';}),
        'action' => 'edit',
        'class' => $class
      ]);
    }

    /**
     * Add a class
     * @param Illuminate\Http\Request $request form params
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request){
      $validator = $this->validate($request, [
        'name' => 'required|max:50',
        'teacher' => 'required|not_in:0',
        'formation' => 'required|not_in:0'
      ]);

      // Check if all data exists
      $teacher = Teacher::findOrFail($request->teacher);
      $formation = Formation::findOrFail($request->formation);

      // Check if class with name already exist
      if (Class_::where('claName', $request->name)->first()){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.modelWithNameAlreadyExists.title'), 'msg' => Lang::get('gest.modelWithNameAlreadyExists.msg', ['model' => 'classe'])]);
        return redirect()->back()->withInput();
      }

      $class = new Class_();
      $class->claName = $request->name;
      $class->teacher()->associate($teacher);
      $class->formation()->associate($formation);
      $class->save();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.addSucces.title'), 'msg' => Lang::get('gest.addSucces.msg', array('model' => $class->claName))]);
      return redirect()->route('classes');
    }

    /**
     * Edit a class
     * @param  Illuminate\Http\Request $request form paramas
     * @param  string|integer  $id      class's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id){
      $validator = $this->validate($request, [
        'name' => 'required|max:50',
        'teacher' => 'required|not_in:0',
        'formation' => 'required|not_in:0'
      ]);

      // Check if all data exists
      $class = Class_::findOrFail($id);
      $teacher = Teacher::findOrFail($request->teacher);
      $formation = Formation::findOrFail($request->formation);

      // Check if class with name already exist
      $classWithName = Class_::where('claName', $request->name)->first();
      if ($classWithName && $classWithName->claName == $request->name && $classWithName != $class){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.modelWithNameAlreadyExists.title'), 'msg' => Lang::get('gest.modelWithNameAlreadyExists.msg', ['model' => 'classe'])]);
        return redirect()->back()->withInput();
      }

      $class->claName = $request->name;
      $class->teacher()->associate($teacher);
      $class->formation()->associate($formation);
      $class->save();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.editSucces.title'), 'msg' => Lang::get('gest.editSucces.msg', array('model' => $class->claName))]);
      return redirect()->route('classes');
    }

    /**
     * Archive a class
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      class's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function archive(Request $request, $id){
      $class = Class_::findOrFail($id);

      $class->claArchived = true;
      $class->save();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.archiveSucces.title'), 'msg' => Lang::get('gest.archiveSucces.msg', array('model' => $class->claName))]);
      return redirect()->route('classes');
    }

    /**
     * Unarchive a class
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      class's id
     * @return \Illuminate\Http\RedirectResponse
     */
     public function unarchive(Request $request, $id){
       $class = Class_::findOrFail($id);

       $class->claArchived = false;
       $class->save();

       $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.unarchiveSucces.title'), 'msg' => Lang::get('gest.unarchiveSucces.msg', array('model' => $class->claName))]);
       return redirect()->route('classes');
     }

     /**
      * Download the view student's follow-up in PDF
      * @param  string|integer $id student's id
      * @param  integer $semester semester to export
      * @return Illuminate\Http\Response     PDF file download
      */
     public function downloadBySemesterPDF($id, $semester){
       $class = Class_::findOrFail($id);

       // Check semester
       if ($class->formation->forYears < $semester/2 || $semester == 0){
         abort(404);
       }

       // Get lessons by category
       $lessonsByCategory = $class->formation->lessons->groupBy('category.catName')->map(function($lesson) { return $lesson->pluck('lesName', 'idLesson'); });

       // Get students's follow-up
       $students = [];
       $i = 0;
       foreach ($class->students as $student){
         $students[$i]['id'] = $student->idStudent;
         $students[$i]['name'] = $student->stuFirstname.' '.$student->stuLastname;
         $students[$i]['followup'] = $student->followUp();
         $i++;
       }

       $pdf = SnappyPdf::loadView('class.pdf-details-semester', [
         'class' => $class,
         'students' => $students,
         'semester' => $semester,
         'lessonsByCategory' => $lessonsByCategory
       ]);

       return $pdf->download($class->claName.'-semester-'.$semester.'.pdf');
     }

     /**
      * Download the view class's follow-up in PDF and group all semesters
      * @param  string|integer $id class's id
      * @return Illuminate\Http\Response     PDF file download
      */
     public function downloadByAllPDF($id){
       $class = Class_::findOrFail($id);

       // Get lessons by category
       $lessonsByCategory = $class->formation->lessons->groupBy('category.catName')->map(function($lesson) { return $lesson->pluck('lesName', 'idLesson'); });

       // Check if have student
       if ($class->students->isEmpty()){
         $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.noStudent.title'), 'msg' => Lang::get('gest.noStudent.msg')]);
         return redirect()->back('viewClass', ['id' => $class->idClass]);
       }

       // Get students's follow-up
       $students = [];
       $i = 0;
       $maxIndex = -1;
       foreach ($class->students as $student){
         $students[$i]['id'] = $student->idStudent;
         $students[$i]['name'] = $student->stuFirstname.' '.$student->stuLastname;
         $students[$i]['followup'] = $student->followUp();

         end($students[$i]['followup']['semesters']['countIndex']);
         $iIndex = key($students[$i]['followup']['semesters']['countIndex']);
         $maxIndex = $iIndex > $maxIndex ? $iIndex : $maxIndex;

         $i++;
       }

      // Check if have semester
      if ($maxIndex === false || $maxIndex === -1){
        $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.noSemester.title'), 'msg' => Lang::get('gest.noSemester.msg')]);
        return redirect()->back('viewClass', ['id' => $class->idClass]);
      }

       $pdf = SnappyPdf::loadView('class.pdf-details-all', [
         'class' => $class,
         'students' => $students,
         'maxIndex' => $maxIndex,
         'lessonsByCategory' => $lessonsByCategory
       ]);

       return $pdf->download($class->claName.'.pdf');
     }
}
