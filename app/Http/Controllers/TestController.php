<?php
/**
 * Author: Loïc Herzig
 * Date: 07.02.2019
 * Description: Test controller
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Auth;

use App\Models\Category;
use App\Models\Class_;
use App\Models\Formation;
use App\Models\Grade;
use App\Models\Lesson;
use App\Models\Student;
use App\Models\Teacher;

class TestController extends Controller
{
  public function test(Request $request){
    
  }
}
