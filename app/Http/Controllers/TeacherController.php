<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 13.02.2019
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Lang;
use App\Models\Teacher;
use App\Models\Class_;

/**
 * Controller for Teacher model
 */
class TeacherController extends Controller
{
    /**
     * Show list of teacher (can also accept search params)
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function showTeachers(Request $request){
      if($request->search) {
         $teachers = Teacher::all()->filter(function ($teacher) use ($request){
           if ($teacher->teaFirstname == 'admin') { return false; }

           $search = strtolower($request->search);
           switch ($request->search) {
              case 'archivé':
                return $teacher->teaArchived;
                break;
              case 'désarchivé':
                return !$teacher->teaArchived;
                break;
              default:
                $fields = $teacher->idTeacher.';'.$teacher->teaAcronym.';'.$teacher->teaFirstname.' '.$teacher->teaLastname.';'.$teacher->classes()->pluck('claName')->implode('/');
                $fields = strtolower($fields);
                return strpos($fields, $search) !== false;
           }
         });
       }
       else {
        $teachers = Teacher::all()->reject(function ($teacher){
          return $teacher->teaFirstname == 'admin';
        });
       }

      return view('teacher.teachers', ['teachers' => $teachers]);
    }

    /**
     * Show add form
     * @return \Illuminate\View\View
     */
    public function showAdd(){
      return view('teacher.form', [
        'action' => 'add'
      ]);
    }

    /**
     * Show edit form
     * @param  integer|string $id teacher's id
     * @return \Illuminate\View\View
     */
    public function showEdit($id){
      $teacher = Teacher::findOrFail($id);

      return view('teacher.form', [
        'action' => 'edit',
        'teacher' => $teacher
      ]);
    }

    /**
     * Add a teacher
     * @param Illuminate\Http\Request $request form params
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request){
      $validator = $this->validate($request, [
        'acronym' => 'required|max:6',
        'firstname' => 'required|max:50',
        'lastname' => 'required|max:50',
      ]);

      // Check if teacher with acronym already exist
      if (Teacher::where('teaAcronym', $request->acronym)->first()){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.teacherWithAcronymAlreadyExists.title'), 'msg' => Lang::get('gest.teacherWithAcronymAlreadyExists.msg')]);
        return redirect()->back()->withInput();
      }

      $teacher = Teacher::create([
        'teaAcronym' => $request->acronym,
        'teaFirstname' => $request->firstname,
        'teaLastname' => $request->lastname,
      ]);
      $teacher->refresh();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.addSucces.title'), 'msg' => Lang::get('gest.addSucces.msg', array('model' => $teacher->teaFirstname.' '.$teacher->teaLastname))]);
      return redirect()->route('teachers');
    }

    /**
     * Edit a teacher
     * @param  Illuminate\Http\Request $request form paramas
     * @param  string|integer  $id      teacher's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id){
      $attributes = [
        'firstname' => 'required|max:50',
        'lastname' => 'required|max:50'
      ];

      if ($request->ajax()){
        $validator = Validator::make($request->all(), $attributes);

        if ($validator->fails()) {
          return response()->json([
            'success' => false,
            'errors'=>$validator->errors()->all()
          ]);
        }
      }
      else {
        $attributes['acronym'] = 'required|max:6';
        $validator = $this->validate($request, $attributes);
      }

      // Check if all data exist
      $teacher = Teacher::findOrFail($id);

      // Check if teacher with the new acronym exists
      $teacherWithAcronym = Teacher::where('teaAcronym', $request->acronym)->first();
      if ($teacherWithAcronym && $teacherWithAcronym->teaAcronym == $request->acronym && $teacherWithAcronym != $teacher){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.teacherWithAcronymAlreadyExists.title'), 'msg' => Lang::get('gest.teacherWithAcronymAlreadyExists.msg')]);
        return redirect()->back()->withInput();
      }

      if (isset($request->acronym)) { $teacher->teaAcronym = $request->acronym; }
      $teacher->teaFirstname = $request->firstname;
      $teacher->teaLastname = $request->lastname;
      $teacher->save();
      $teacher->refresh();

      if ($request->ajax()){
        return response()->json([
          'success'=> true,
          'msg' => Lang::get('gest.editSucces.title').' '.Lang::get('gest.editSucces.msg', ['model' => $teacher->teaFirstname.' '.$teacher->teaLastname])
        ]);
      }

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.editSucces.title'), 'msg' => Lang::get('gest.editSucces.msg', array('model' => $teacher->teaFirstname.' '.$teacher->teaLastname))]);
      return redirect()->route('teachers');
    }

    /**
     * Archive a teacher
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      teacher's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function archive(Request $request, $id){
      // Check if all data exist
      $teacher = Teacher::findOrFail($id);

      $teacher->teaArchived = true;
      $teacher->save();
      $teacher->refresh();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.archiveSucces.title'), 'msg' => Lang::get('gest.archiveSucces.msg', array('model' => $teacher->teaFirstname.' '.$teacher->teaLastname))]);
      return redirect()->route('teachers');
    }

    /**
     * Unarchive a teacher
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      teacher's id
     * @return \Illuminate\Http\RedirectResponse
     */
     public function unarchive(Request $request, $id){
       // Check if all data exist
       $teacher = Teacher::findOrFail($id);

       $teacher->teaArchived = false;
       $teacher->save();
       $teacher->refresh();

       $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.unarchiveSucces.title'), 'msg' => Lang::get('gest.unarchiveSucces.msg', array('model' => $teacher->teaFirstname.' '.$teacher->teaLastname))]);
       return redirect()->route('teachers');
     }
}
