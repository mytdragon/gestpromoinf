<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 08.02.2019
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Models\Lesson;
use App\Models\Category;
use App\Models\Formation;

/**
 * Controller for Lesson model
 */
class LessonController extends Controller
{
    /**
     * Show list of lesson (can also accept search params)
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function showLessons(Request $request){
      if($request->search) {
         $lessons = Lesson::all()->filter(function ($lesson) use ($request){
           $search = strtolower($request->search);
           switch ($request->search) {
              case 'archivé':
                return $lesson->lesArchived;
                break;
              case 'désarchivé':
                return !$lesson->lesArchived;
                break;
              default:
                $fields = $lesson->idLesson.';'.$lesson->lesName.';'.$lesson->category->catName;
                $fields = strtolower($fields);
                return strpos($fields, $search) !== false;
           }
         });
       }
       else {
        $lessons = Lesson::all();
       }

      return view('lesson.lessons', ['lessons' => $lessons]);
    }

    /**
     * Show add form
     * @return \Illuminate\View\View
     */
    public function showAdd(){
      return view('lesson.form', [
        'categories' => Category::all(),
        'formations' => Formation::all(),
        'action' => 'add'
      ]);
    }

    /**
     * Show edit form
     * @param  integer|string $id lesson's id
     * @return \Illuminate\View\View
     */
    public function showEdit($id){
      $lesson = Lesson::findOrFail($id);

      return view('lesson.form', [
        'categories' => Category::all(),
        'formations' => Formation::all(),
        'action' => 'edit',
        'lesson' => $lesson
      ]);
    }

    /**
     * Add a lesson
     * @param Illuminate\Http\Request $request form params
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request){
      $validator = $this->validate($request, [
        'name' => 'required|max:50',
        'category' => 'required|not_in:0',
        'formations' => 'array'
      ]);

      // Check if all data exists
      $category = Category::findOrFail($request->category);
      $formations = collect($request->formations)->map(function ($id) {
        return Formation::findOrFail($id)->idFormation;
      });

      // Check if lesson with name already exist
      if (Lesson::where('lesName', $request->name)->first()){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.modelWithNameAlreadyExists.title'), 'msg' => Lang::get('gest.modelWithNameAlreadyExists.msg', ['model' => 'cours'])]);
        return redirect()->back()->withInput();
      }

      $lesson = new Lesson();
      $lesson->lesName = $request->name;
      $lesson->category()->associate($category);
      $lesson->save();

      $lesson->formations()->attach($formations->toArray());

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.addSucces.title'), 'msg' => Lang::get('gest.addSucces.msg', array('model' => $lesson->lesName))]);
      return redirect()->route('lessons');
    }

    /**
     * Edit a lesson
     * @param  Illuminate\Http\Request $request form paramas
     * @param  string|integer  $id      lesson's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id){
      $validator = $this->validate($request, [
        'name' => 'required|max:50',
        'category' => 'required|not_in:0',
        'formations' => 'array'
      ]);

      // Check if all data exists
      $lesson = Lesson::findOrFail($id);
      $category = Category::findOrFail($request->category);
      $formations = collect($request->formations)->map(function ($id) {
        return Formation::findOrFail($id)->idFormation;
      });

      // Check if lesson with name already exist
      $lessonWithName = Lesson::where('lesName', $request->name)->first();
      if ($lessonWithName && $lessonWithName->lesName == $request->name && $lessonWithName != $lesson){
        $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.modelWithNameAlreadyExists.title'), 'msg' => Lang::get('gest.modelWithNameAlreadyExists.msg', ['model' => 'cours'])]);
        return redirect()->back()->withInput();
      }

      $lesson->lesName = $request->name;
      $lesson->category()->associate($category->idCategory);
      $lesson->save();

      $lesson->formations()->detach();
      $lesson->formations()->attach($formations->toArray());

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.editSucces.title'), 'msg' => Lang::get('gest.editSucces.msg', array('model' => $lesson->lesName))]);
      return redirect()->route('lessons');
    }

    /**
     * Archive a lesson
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      lesson's id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function archive(Request $request, $id){
      $lesson = Lesson::findOrFail($id);

      $lesson->lesArchived = true;
      $lesson->save();

      $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.archiveSucces.title'), 'msg' => Lang::get('gest.archiveSucces.msg', array('model' => $lesson->lesName))]);
      return redirect()->route('lessons');
    }

    /**
     * Unarchive a lesson
     * @param  Illuminate\Http\Request $request
     * @param  string|integer  $id      lesson's id
     * @return \Illuminate\Http\RedirectResponse
     */
     public function unarchive(Request $request, $id){
       $lesson = Lesson::findOrFail($id);

       $lesson->lesArchived = false;
       $lesson->save();

       $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.unarchiveSucces.title'), 'msg' => Lang::get('gest.unarchiveSucces.msg', array('model' => $lesson->lesName))]);
       return redirect()->route('lessons');
     }
}
