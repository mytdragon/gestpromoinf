<?php

/**
 * ETML
 * Author: Loïc Herzig
 * Date: 14.03.2019
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use Validator;
use Lang;

use App\Models\Student;
use App\Models\Teacher;
use App\Models\Class_;
use App\Models\Formation;
use App\Models\Lesson;
use App\Models\Grade;

/**
 * Import excel file related controller
 */
class ImportController extends Controller
{
  /**
   * Import file excel (xlsx, xlsm)
   * @param  Request $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View           Redirect to the class details or to the conflicts view
   */
  public function import(Request $request){
    $validator = Validator::make($request->all(), [
      'semester' => 'required|integer',
      'file' => 'required|file|mimes:xlsx,xlsm',
    ]);

    // Format error message if validator fails
    if ($validator->fails()) {
      $request->session()->flash('message', ['result' => false, 'title' => 'Echec!', 'msg' => implode($validator->errors()->all(),' ')]);
      return redirect()->route('classes');
    }

    $semester = $request->semester;

   // Get file from request
   $file = $request->file('file');

   $array = Self::toArray($file);// NOTE we cannot get semester
   $conflicts = [];

   // Check if teacher exists and create him if not exist
   if ($array['class']['teacher'] == 'admin'){ abort(403); }
   $teacher = Teacher::where('teaAcronym', $array['class']['teacher'])->first();

   if (!$teacher){
     $teacher = Teacher::create([
       'teaUsername' => null,
       'teaPassword' => null,
       'teaAcronym' => strtoupper($array['class']['teacher']),
       'teaFirstname' => '',
       'teaLastname' => ''
     ]);
     $teacher->refresh();
     $conflicts['teacher'] = $teacher->idTeacher;
   }

   // Check if formation exists
   $formation = Formation::where('forName', $array['class']['formation'])->first();
   if (!$formation){
     throw new \Exception("Formation {$array['class']['formation']} does not exists", 1);
   }

   // Check if class exists
   $class = Class_::where('claName', $array['class']['name'])->first();
   if (!$class){
     $class = new Class_();
     $class->claName = $array['class']['name'];
     $class->teacher()->associate($teacher);
     $class->formation()->associate($formation);
     $class->save();
   }

   foreach ($array['students'] as $student){
     // Check if student exists
     $studentModel = Student::all()->filter(function ($student_) use ($student) {
       return $student_->stuFirstname == $student['firstname'] && $student_->stuLastname == $student['lastname'];
     })->first();

     // Create student if not found
     if (!$studentModel){
       $studentModel = new Student();
       $studentModel->stuFirstname = $student['firstname'];
       $studentModel->stuLastname = $student['lastname'];
       $studentModel->class()->associate($class);
       $studentModel->save();
       $studentModel->refresh();
     }

     // Grades
     foreach ($student['lessons'] as $lesson => $number){
       $lesson = Lesson::where('lesName', $lesson)->firstOrFail();

       $grade = $studentModel->grades
       ->where('fkLesson', $lesson->idLesson)
       ->where('graSemester', $semester)
       ->where('graRepetition', false)
       ->first();

       if ($grade) {
         // Add conflict if a grade exists
         if(!isset($conflicts['student'][$studentModel->idStudent])){
           $conflicts['student'][$studentModel->idStudent]['student'] = [
               'firstname' => $studentModel->stuFirstname,
               'lastname' => $studentModel->stuLastname
            ];
         }

         $conflicts['student'][$studentModel->idStudent]['conflicts'][] = [
           'semester' => $semester,
           'grade' => [
               'id' => $grade->idGrade,
               'number' => $grade->graNumber,
               'newGrade' => $number,
               'lesson' => $lesson->lesName,
           ],
         ];
       }
       else {
         if ($number){
           $grade = new Grade();
           $grade->graNumber = $number;
           $grade->graSemester = $semester;
           $grade->student()->associate($studentModel);
           $grade->lesson()->associate($lesson);
           $grade->save();
         }
       }
     }
   }

   if ($conflicts != []){
     return view('class.post-import', ['conflicts' => $conflicts, 'route' => route('viewClass', ['id' => $class->idClass, 'semester' => $semester])]);
   }
   else {
     $request->session()->flash('message', ['result' => true,'title' => Lang::get('gest.importationDone.title'), 'msg' => Lang::get('gest.importationDone.msg')]);
     return redirect()->route('viewClass', ['id' => $class->idClass, 'semester' => $semester]);
   }
  }

  /**
  * Read the excel file (XLSX or XLSM) and create an array
  * @param  object $file excel file to convert
  * @return array       formated array of students
  */
  private function toArray($file) {
    $importConfig = config('import');
    $spreadsheet = IOFactory::load($file);
    $worksheet = $spreadsheet->getSheetByName($importConfig['sheetName']);

    if (!$worksheet){
      throw new \Exception("Sheet not found with name '{$importConfig['sheetName']}'", 1);
    }

    $array = []; // array returned

    $teacher = $worksheet->getCell($importConfig['teacher'])->getFormattedValue();

    if ($teacher == '' || $teacher == null){
      throw new \Exception("L'acronyme de l'enseignant n'est pas défini", 1);
    }

    $class = $worksheet->getCell($importConfig['class'])->getFormattedValue();
    $formation = strtoupper(preg_replace('/[0-9]/', '', $class));
    $formation = Formation::where('forName', $formation)->firstOrFail();

    $array['class'] = [
     'name' => $class,
     'formation' => $formation->forName,
     'teacher' => $teacher
    ];

    // Find the number of student and add students
    $numberStudentFound = false;
    $nbStudents = 0;
    $rowIndex = $importConfig['firstStudentRow'];
    do {
     switch ($worksheet->getCell($importConfig['columnToStopStudent'].$rowIndex)->getFormattedValue()) {
       case '':
         $numberStudentFound = true;
         break;

       case $importConfig['textForStopStudent']:
         $numberStudentFound = true;
         break;

       default:
         $no = $worksheet->getCell($importConfig['students']['no'].$rowIndex)->getFormattedValue();
         $firstname = $worksheet->getCell($importConfig['students']['firstname'].$rowIndex)->getFormattedValue();
         $lastname = $worksheet->getCell($importConfig['students']['lastname'].$rowIndex)->getFormattedValue();
         $array['students'][$no] = [
           'firstname' => $firstname,
           'lastname' => $lastname
         ];
         $nbStudents++;
         break;
     }
     $rowIndex++;
    } while (!$numberStudentFound);

    $lessonRow = $importConfig['lessonsRow'];
    $row = $worksheet->getRowIterator()->seek($lessonRow)->current();
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(FALSE);

    // Get formation's lesson
    $formationsLessons = $formation->lessons->pluck('lesName');

    foreach ($cellIterator as $cell) {
      // Check if cell is a lesson
      if ($formationsLessons->contains($cell->getFormattedValue())){
        $studentNo = 1;
        $column = $cell->getColumn();
        for ($i = $importConfig['firstStudentRow']; $i < $importConfig['firstStudentRow'] + $nbStudents; $i++){
          $array['students'][$studentNo]['lessons'][$cell->getFormattedValue()] = $worksheet->getCell($column.$i)->getFormattedValue();
          $studentNo++;
        }
      }
    }

    return $array;
  }

  /**
   * Replace the number of a grade
   * @param  Request $request AJAX's request
   * @return Illuminate\Http\Response           JSON result
   */
  public function replaceGrade(Request $request){
    $validator = Validator::make($request->all(), [
      'changes' => 'required|array',
      'changes.*.oldGrade' => 'required|integer',
      'changes.*.newGrade' => 'nullable|between:1,6'
    ]);

    if ($validator->fails()) {
      return response()->json([
        'success' => false,
        'errors'=>$validator->errors()->all()
      ]);
    }

    foreach ($request->changes as $change){
      // Edit grade number
      $grade = Grade::findOrFail($change['oldGrade']);

      // If new grade is null, remove. Edit if not
      if ($change['newGrade']){
        $grade->graNumber = $change['newGrade'];
        $grade->save();
      }
      else {
        $grade->delete();
      }
    }

    return response()->json([
      'success'=> true,
    ]);
  }

  /**
   * Create a repeat grade or edit repeated grade if one already exist
   * @param  Request $request AJAX's request
   * @return Illuminate\Http\Response           JSON result
   */
  public function repeatGrade(Request $request){
    $validator = Validator::make($request->all(), [
      'changes' => 'required|array',
      'changes.*.oldGrade' => 'required|integer',
      'changes.*.newGrade' => 'nullable|between:1,6'
    ]);

    if ($validator->fails()) {
      return response()->json([
        'success' => false,
        'errors'=>$validator->errors()->all()
      ]);
    }

    foreach ($request->changes as $change){
      // Get current grade
      $grade = Grade::findOrFail($change['oldGrade']);

      // if grade exist in repeat, edit the existing one, else create new
      $gradeRepeat = Grade::where('fkStudent', $grade->student->idStudent)
        ->where('fkLesson', $grade->lesson->idLesson)
        ->where('graSemester', $grade->graSemester)
        ->where('graRepetition', true)
        ->first();

      if ($gradeRepeat) {
        $gradeRepeat->graNumber = $change['newGrade'];
        $gradeRepeat->save();
      }
      else {
        $newGrade = new Grade();
        $newGrade->graNumber = $change['newGrade'];
        $newGrade->graSemester = $grade->graSemester;
        $newGrade->graRepetition = true;
        $newGrade->student()->associate($grade->student);
        $newGrade->lesson()->associate($grade->lesson);
        $newGrade->save();
      }
    }

    return response()->json([
      'success'=> true,
    ]);
  }
}
