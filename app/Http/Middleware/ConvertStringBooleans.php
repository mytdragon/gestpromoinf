<?php
/**
 * Author: jfadich
 * Source: https://github.com/laravel/ideas/issues/514#issuecomment-299038674
 * Date: 13.02.2019
 */

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

/**
 * Convert string to boolean in request
 */
class ConvertStringBooleans extends TransformsRequest
{
    protected function transform($key, $value)
    {
        if($value === 'true' || $value === 'TRUE')
            return true;

        if($value === 'false' || $value === 'FALSE')
            return false;

        return $value;
    }
}
