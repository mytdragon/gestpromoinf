<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;

class Promotion extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idPromotion', 'proName'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_promotion';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idPromotion';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The students that have the promotion.
   *
   * By default ordered by student's id ascending
   *
   * @return array Student
   */
  public function students() {
      return $this->belongsToMany(Student::class,'t_student_promotion','fkPromotion','fkStudent')->orderBy('idStudent','asc');
  }
}
