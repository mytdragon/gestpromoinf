<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for formations

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lesson;
use App\Models\Class_;

class Formation extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idFormation', 'forName', 'forYears', 'forArchived'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_formation';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idFormation';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The lessons that belong to the formation.
   *
   * By default ordered by lesson's name ascending
   *
   * @return array Lesson
   */
  public function lessons() {
      return $this->belongsToMany(Lesson::class,'t_formation_lesson','fkFormation','fkLesson')->orderBy('lesName','asc');
  }

  /**
   * The classes for the formation
   *
   * By default ordered by class's name ascending
   *
   * @return array Class_
   */
  public function classes(){
      return $this->hasMany(Class_::class,'fkFormation','idFormation')->orderBy('claName','desc');
  }
}
