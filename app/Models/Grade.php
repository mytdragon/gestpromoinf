<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for grades

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\Lesson;

class Grade extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idGrade', 'graNumber', 'graSemester', 'graRepetition'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_grade';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idGrade';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The student to which the grade belongs
   *
   * @return array Student
   */
  public function student() {
      return $this->belongsTo(Student::class,'fkStudent','idStudent');
  }

  /**
   * The lesson to which the grade belongs
   *
   * @return array Lesson
   */
  public function lesson() {
      return $this->belongsTo(Lesson::class,'fkLesson','idLesson');
  }
}
