<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for teachers

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Encryptable;
use App\Models\Class_;

class Teacher extends Authenticatable
{
  use Encryptable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idTeacher', 'teaUsername', 'teaPassword', 'teaAcronym', 'teaFirstname', 'teaLastname', 'teaIsAdmin', 'teaArchived'
  ];

  /**
   * The attributes that are encrypted
   *
   * @var array
   */
  protected $encryptable = ['teaFirstname', 'teaLastname'];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_teacher';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idTeacher';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
    * Get the password for the user.
    *
    * @return string
    */
  public function getAuthPassword() {
      return $this->teaPassword;
  }

  /**
   * The classes in which the teacher is the main teacher
   *
   * By default ordered by class's name ascending
   *
   * @return array Class_
   */
  public function classes() {
      return $this->hasMany(Class_::class,'fkTeacher', 'idTeacher')->orderBy('claName','asc');
  }

  /**
   * Overrides the method to ignore the remember token.
   */
  public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute)
    {
      parent::setAttribute($key, $value);
    }
  }
}
