<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for classes

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Teacher;
use App\Models\Formation;
use App\Models\Student;

class Class_ extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idClass', 'claName', 'claArchived'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_class';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idClass';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The teacher to which the class belongs
   *
   * @return array Teacher
   */
  public function teacher() {
      return $this->belongsTo(Teacher::class,'fkTeacher','idTeacher');
  }

  /**
   * The formation to which the class belongs
   *
   * @return array Formation
   */
  public function formation() {
      return $this->belongsTo(Formation::class,'fkFormation','idFormation');
  }

  /**
   * The students in the class
   *
   * By default ordered by lastname ascending
   *
   * @return array Student
   */
  public function students(){
      return $this->hasMany(Student::class,'fkClass','idClass')->orderBy('stuLastname','asc');
  }
}
