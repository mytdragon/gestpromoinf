<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for students

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use Illuminate\Support\Arr;
use App\Traits\Encryptable;
use App\Models\Class_;
use App\Models\Grade;
use App\Models\Promotion;

class Student extends Model
{
  use Encryptable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idStudent', 'stuFirstname', 'stuLastname', 'stuConditional', 'stuRepetition', 'stuArchived'
  ];

  /**
   * The attributes that are encrypted
   *
   * @var array
   */
  protected $encryptable = ['stuFirstname', 'stuLastname'];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_student';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idStudent';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The class of the student
   *
   * By default ordered by class's name ascending
   *
   * @return array Class_
   */
  public function class() {
      return $this->belongsTo(Class_::class,'fkClass','idClass')->orderBy('claName', 'asc');
  }

  /**
   * The grades of the student
   *
   * By default ordered by semesters descending
   *
   * @return array Grade
   */
  public function grades(){
      return $this->hasMany(Grade::class,'fkStudent','idStudent')->orderBy('graSemester','desc');
  }

  /**
   * The promotions that the student have.
   *
   * By default ordered by semster ascending then repetition
   *
   * @return array Promotion
   */
  public function promotions() {
      return $this->belongsToMany(Promotion::class,'t_student_promotion','fkStudent','fkPromotion')->orderBy('sprSemester','asc')->orderBy('sprRepetition','asc')
        ->withPivot('idStudentPromo', 'sprSemester', 'sprRepetition');
  }

  /**
   * Get the followUp for the student with grades, average, promotion, semesters done
   *
   * The grades are ordered by Semester (asc) (repetition format: 1' 2' 3' ...)
   */
  public function followUp($repeatAdded = null){
    // Formated array that will be returned
    $gradesFormated = [];

    // Get all grades for student
    $grades = $this->grades;

    // Get semester possible
    $semestersPossible = [];
    for ($i = 1; $i <= $this->class->formation->forYears * 2; $i++){
      $semestersPossible[] = "$i";
      $semestersPossible[] = "$i'";
    }
    $gradesFormated['semesters']['no'] = $semestersPossible;

    // Get higher semester grade and check if he has a grade
    $higherSemGrade = $grades->sortByDesc('graSemester')->first();
    $semestersDone = [];

    $higherSem = 0;
    if ($higherSemGrade){
      // Get higher semester and semesters
      $higherSem = $higherSemGrade->graSemester;

      for ($i = $higherSem; $i > 0; $i--){
        $semestersDone[] = "{$i}";

        $semesterBis = $grades->where('graSemester', $i)->where('graRepetition', 1)->first();
        if ($semesterBis) { $semestersDone[] = $semesterBis->graSemester.'\''; }
      }
    }

    // Check if there is a higher semester in promotions
    $promotions = $this->promotions;
    $higherPromotion = $promotions->sortByDesc('pivot.sprSemester')->first();
    if ($higherPromotion && $higherPromotion->pivot->sprSemester > $higherSem){
      $difSemester = $higherPromotion->pivot->sprSemester - $higherSem;
      for ($i = $difSemester; $i > 0; $i--){
        $semester = $higherSem + $i;
        $semestersDone[] = "{$semester}";

        $semesterBis = $promotions->where('pivot.sprSemester', $semester)->where('pivot.sprRepetition', 1)->first();
        if ($semesterBis) { $semestersDone[] = $semester.'\''; }
      }
    }
    else {
      // Check if there is a promotion with repeated semester
      $semesterBis = $promotions->where('pivot.sprSemester', $higherSem)->where('pivot.sprRepetition', 1)->first();
      if ($semesterBis) { $semestersDone[] = $higherSem.'\''; }
    }

    // Add the repeated semester if it's needed to add one
    if ($repeatAdded != null && $repeatAdded > 0 && count(Arr::where($semestersDone, function ($value, $key) { return str_contains($value, '\''); })) < 2) {
      // check if he has done the "normal" semester
      if (!in_array($repeatAdded,$semestersDone)) { throw new \Exception(Lang::get('gest.normalSemesterNotDone.msg', ['semester' => $repeatAdded]), 1); }
      $semestersDone[] = $repeatAdded.'\'';
    }

    sort($semestersDone);

    // get index semester repeated
    $repeatedSemesters = [];

    // Define semesters that count, get index semester repeated and innactive
    $indexsSemesterCount = [];
    $repeatFound = 0;
    for ($i = 0; $i < count($semestersDone);$i++){
      if (str_contains($semestersDone[$i], '\'')){
        // Remove last semester of array
        array_pop($indexsSemesterCount);
        $repeatedSemesters[] = $semestersDone[$i - 1];
        $repeatFound++;

        // If there is no more repeated semester and repeat semester found is one, exit the loop
        if ($repeatFound == 1 && $i+2 < count($semestersDone) && !str_contains($semestersDone[$i+2], '\'')){
          $indexsSemesterCount[] = array_search($semestersDone[$i], $semestersPossible);
          break;
        }
      }

      $indexsSemesterCount[] = array_search($semestersDone[$i], $semestersPossible);
    }

    $innactivesSemesters = array_values(array_diff($gradesFormated['semesters']['no'], $semestersDone));

    // Remove the last semesters if he is repeating the penultimate one
    if(!str_contains($gradesFormated['semesters']['no'][end($indexsSemesterCount)],'\'') && $repeatFound == 1 ){
      /*$repeatedSemesters[] = $gradesFormated['semesters']['no'][end($indexsSemesterCount)];
      array_pop($indexsSemesterCount);*/
    }

    if ($repeatFound == 1){
      // If indexRepeatFound + 1 = last of indexCount -> remove only last
      if (array_search(end($repeatedSemesters).'\'', $semestersPossible) + 1 == end($indexsSemesterCount)){
        $repeatedSemesters[] = $gradesFormated['semesters']['no'][end($indexsSemesterCount)];
        array_pop($indexsSemesterCount);
      }
      else { // Else add the next semester in repeat
        $repeatedSemesters[] = $repeatedSemesters[0] + 1;
      }

    }

    // Get semesters done index
    $semestersDoneIndexs = [];
    foreach ($semestersDone as $semester){
      $semestersDoneIndexs[] = array_search($semester, $semestersPossible);
    }

    $gradesFormated['semesters']['countIndex'] = $indexsSemesterCount;
    $gradesFormated['semesters']['repeated'] = $repeatedSemesters;
    $gradesFormated['semesters']['innactives'] = $innactivesSemesters;
    $gradesFormated['semesters']['done'] = $semestersDone;
    $gradesFormated['semesters']['doneIndexs'] = $semestersDoneIndexs;

    // PROMOTIONS
    $promotionsBySemester = [];
    foreach ($semestersPossible as $semester){
      $repeat = str_contains($semester, '\'');
      $semesterNo = str_replace('\'', '', $semester);
      $promotion = $promotions->where('pivot.sprSemester', '=', $semesterNo)->where('pivot.sprRepetition', '=', $repeat)->first();
      $promotionsBySemester[$semester] = $promotion ? ['id' => $promotion->idPromotion, 'name' => $promotion->proName] : null;
    }
    $gradesFormated['promotions'] = $promotionsBySemester;

    // Get grade by lesson by category
    $lessonsByCategory = $this->class->formation->lessons->groupBy('category.catName')->map(function($lesson) { return $lesson->pluck('lesName', 'idLesson'); });
    $gradesFormated['categories'] = [];

    foreach ($lessonsByCategory as $category => $lessons){
      $sortedLesson = [];
      foreach ($lessons as $id => $lesson){
        $sortedGrades = [];
        foreach ($semestersPossible as $semester) {
          if (str_contains($semester, '\'')){
            $grade = $grades->where('graSemester', str_replace_first('\'', '', $semester))->where('graRepetition', 1)->where('fkLesson', $id)->first();
            $sortedGrades[] = $grade ? $grade->graNumber : null;
          } else {
            $grade = $grades->where('graSemester', $semester)->where('graRepetition', 0)->where('fkLesson', $id)->first();
            $sortedGrades[] = $grade ? $grade->graNumber : null;
          }
        }
        $sortedLesson[$lesson]['grades'] = $sortedGrades;
      }
      $gradesFormated['categories'][$category]['lessons'] = $sortedLesson;
    }

    // Now we have all the grades, we can calc average

    /* SEMESTER
    // --------
    // CFC:
    // ecg
    // cbe
    // modules: 80% I, 20% C (+ I and C individual)
    // prat
    // global cfc: 30% modules, 30% prat, 20% ecg, 20% cbe
    //
    // MPTi:
    // mpt
    // scinat
    // modules: 80% I, 20% C (+ I and C individual)
    // prat
    // global cfc: 50% modules, 50% prat
    //
    // FPA:
    // modules: 80% I, 20% C (+ I and C individual)
    // prat
    // global cfc: 50% modules, 50% prat
    */
    for ($i = 0; $i < count($semestersPossible); $i++){
      // Modules I
      $modulesIGrades = [];
      foreach ($gradesFormated['categories']['Modules I']['lessons'] as $moduleI => $moduleIDetails){
        $grade = $moduleIDetails['grades'][$i];
        if ($grade != '') { $modulesIGrades[] = $grade; }
      }
      $modulesIAvg = collect($modulesIGrades)->avg();
      $gradesFormated['categories']['Modules I']['avgSemesters'][] = $modulesIAvg;

      // Modules C
      $modulesCGrades = [];
      foreach ($gradesFormated['categories']['Modules C']['lessons'] as $moduleC => $moduleCDetails){
        $grade = $moduleCDetails['grades'][$i];
        if ($grade != '') { $modulesCGrades[] = $grade; }
      }
      $modulesCAvg = collect($modulesCGrades)->avg();
      $gradesFormated['categories']['Modules C']['avgSemesters'][] = $modulesCAvg;

      // Modules
      if ($modulesIAvg && $modulesCAvg){
        $avgModules = roundTenth($modulesIAvg)*0.8 + roundTenth($modulesCAvg)*0.2;
      }
      else if ($modulesIAvg){
        $avgModules = $modulesIAvg;
      }
      else if ($modulesCAvg){
        $avgModules = $modulesCAvg;
      }
      else {
        $avgModules = null;
      }
      $gradesFormated['otherAverages']['semesters']['modules'][] = $avgModules;

      // PRAT
      $avgPrat = $gradesFormated['categories']['Pratique']['lessons']['PRAT']['grades'][$i];
      $gradesFormated['categories']['Pratique']['avgSemesters'][] = $avgPrat;

      // Switch specs. formation
      switch ($this->class->formation->forName) {
        case 'MIN':
        case 'MID':
          // MPT
          $mptGrades = [];
          foreach ($gradesFormated['categories']['Théorie MPT']['lessons'] as $mptLesson => $mptLessonDetails){
            $grade = $mptLessonDetails['grades'][$i];
            if ($grade != '') { $mptGrades[] = $grade; }
          }
          $mptAvg = collect($mptGrades)->avg();
          $gradesFormated['categories']['Théorie MPT']['avgSemesters'][] = $mptAvg;

          // Naturals sciences
          $gradesFormated['categories']['Sciences naturelles']['avgSemesters'][] = $gradesFormated['categories']['Théorie MPT']['lessons']['SCINAT']['grades'][$i];

          // Global CFC
          if ($avgModules && $avgPrat){
            $avgGlobalCFC = roundTenth($avgModules)*0.5 + roundTenth($avgPrat)*0.5;
          }
          else {
            $avgGlobalCFC = null;
          }
          $gradesFormated['otherAverages']['semesters']['globalCFC'][] = $avgGlobalCFC;
          break;

        case 'CIN':
        case 'CID':
          // ECG
          $ecgGrades = [];
          foreach ($gradesFormated['categories']['ECG']['lessons'] as $ecgLesson => $ecgLessonDetails){
            $grade = $ecgLessonDetails['grades'][$i];
            if ($grade != '') { $ecgGrades[] = $grade; }
          }
          $ecgAvg = collect($ecgGrades)->avg();
          $gradesFormated['categories']['ECG']['avgSemesters'][] = $ecgAvg;

          // CBE
          $cbeGrades = [];
          foreach ($gradesFormated['categories']['CBE']['lessons'] as $cbeLesson => $cbeLessonDetails){
            $grade = $cbeLessonDetails['grades'][$i];
            if ($grade != '') { $cbeGrades[] = $grade; }
          }
          $cbeAvg = collect($cbeGrades)->avg();
          $gradesFormated['categories']['CBE']['avgSemesters'][] = $cbeAvg;

          // Global CFC
          if ($avgModules && $avgPrat && $ecgAvg && $cbeAvg){
            $avgGlobalCFC = roundTenth($avgModules)*0.3 + roundTenth($avgPrat)*0.3 + roundTenth($ecgAvg)*0.2 + roundTenth($cbeAvg)*0.2;
          }
          else {
            $avgGlobalCFC = null;
          }
          $gradesFormated['otherAverages']['semesters']['globalCFC'][] = $avgGlobalCFC;
          break;

        case 'FIN':
          // Global CFC
          if ($avgModules && $avgPrat){
            $avgGlobalCFC = roundTenth($avgModules)*0.5 + roundTenth($avgPrat)*0.5;
          }
          else {
            $avgGlobalCFC = null;
          }
          $gradesFormated['otherAverages']['semesters']['globalCFC'][] = $avgGlobalCFC;
          break;

        default:
          abort(404);// Formation's not expected
          break;
      }
    }

    /* ANNUAL
    // --------
    // CFC:
    // Ecg (avg sem 1 + 2 / 2)
    // CBE (avg sem 1 + 2 / 2)
    //
    // MPT
    // Mpt (avg sem 1 + 2 / 2)
    //
    // modules (avg modules sem 1 + 2 / 2)
    // global cfc (avg global cfc sem 1 + 2 / 2)
    */
    $nbYear = count($indexsSemesterCount)/2;
    $gradesFormated['years']['count'] = ceil($nbYear);
    $finishedYears = floor($nbYear);
    $isYearFinished = $nbYear - $finishedYears == 0;
    $firstRepeatYearFound = false;
    $countOnlyLastSem = false;
    $repeatFound = 0;

    $indexsForYear = 0;
    for ($i = 1; $i <= $finishedYears; $i++){
      // find if repeat on odd semester and need to count second semester only
      $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];

      // If first semester repeated found, next one should be repeated and count both semester
      if (str_contains($gradesFormated['semesters']['no'][$semIndex], '\'')){ $repeatFound++; }

      // If semester 2 of year is repeated, if there is no repeat semester, count only last semester of year
      if (str_contains($gradesFormated['semesters']['no'][$gradesFormated['semesters']['countIndex'][$indexsForYear + 1]], '\'')){
        if ($repeatFound == 0) { $countOnlyLastSem = true; }
        $repeatFound++;
      }

      switch ($this->class->formation->forName) {
        case 'MIN':
        case 'MID':
          if (!$countOnlyLastSem){
            // Get grade semester 1 of year
            $avgModulesSem1 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avgGlobalCFCSem1 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $avgMptSem1 = $gradesFormated['categories']['Théorie MPT']['avgSemesters'][$semIndex];
            $avPratSem1 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $indexsForYear++;

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $avgModulesSem2 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avgGlobalCFCSem2 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $avgMptSem2 = $gradesFormated['categories']['Théorie MPT']['avgSemesters'][$semIndex];
            $avPratSem2 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $indexsForYear++;

            $gradesFormated['otherAverages']['years']['modules'][$i] = collect([roundTenth($avgModulesSem1), roundTenth($avgModulesSem2)])->avg();
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = collect([roundTenth($avgGlobalCFCSem1), roundTenth($avgGlobalCFCSem2)])->avg();
            $gradesFormated['categories']['Théorie MPT']['avgYears'][$i] = collect([roundTenth($avgMptSem1), roundTenth($avgMptSem2)])->avg();
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = collect([roundTenth($avPratSem1),roundTenth($avPratSem2)])->avg();
          }
          else {
            $indexsForYear++;// Go to second semester

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $gradesFormated['otherAverages']['years']['modules'][$i] = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $gradesFormated['categories']['Théorie MPT']['avgYears'][$i] = $gradesFormated['categories']['Théorie MPT']['avgSemesters'][$semIndex];
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $indexsForYear++;
          }
          break;

        case 'CIN':
        case 'CID':
          if (!$countOnlyLastSem){
            // Get grade semester 1 of year
            $avgModulesSem1 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avgGlobalCFCSem1 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $avgEcgSem1 = $gradesFormated['categories']['ECG']['avgSemesters'][$semIndex];
            $avgCbeSem1 = $gradesFormated['categories']['CBE']['avgSemesters'][$semIndex];
            $avPratSem1 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $indexsForYear++;

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $avgModulesSem2 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avPratSem2 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $avgGlobalCFCSem2 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $avgEcgSem2 = $gradesFormated['categories']['ECG']['avgSemesters'][$semIndex];
            $avgCbeSem2 = $gradesFormated['categories']['CBE']['avgSemesters'][$semIndex];
            $indexsForYear++;

            $gradesFormated['otherAverages']['years']['modules'][$i] = collect([roundTenth($avgModulesSem1),roundTenth($avgModulesSem2)])->avg();
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = collect([roundTenth($avgGlobalCFCSem1),roundTenth($avgGlobalCFCSem2)])->avg();
            $gradesFormated['categories']['ECG']['avgYears'][$i] = collect([roundTenth($avgEcgSem1),roundTenth($avgEcgSem2)])->avg();
            $gradesFormated['categories']['CBE']['avgYears'][$i] = collect([roundTenth($avgCbeSem1),roundTenth($avgCbeSem2)])->avg();
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = collect([roundTenth($avPratSem1),roundTenth($avPratSem2)])->avg();
          }
          else {
            $indexsForYear++;// Go to second semester

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $gradesFormated['otherAverages']['years']['modules'][$i] = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $gradesFormated['categories']['ECG']['avgYears'][$i] = $gradesFormated['categories']['ECG']['avgSemesters'][$semIndex];
            $gradesFormated['categories']['CBE']['avgYears'][$i] = $gradesFormated['categories']['CBE']['avgSemesters'][$semIndex];
            $indexsForYear++;
          }
          break;

        case 'FIN':
          if (!$countOnlyLastSem){
            // Get grade semester 1 of year
            $avgModulesSem1 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avPratSem1 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $avgGlobalCFCSem1 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $indexsForYear++;

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $avgModulesSem2 = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $avPratSem2 = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $avgGlobalCFCSem2 = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $indexsForYear++;

            $gradesFormated['otherAverages']['years']['modules'][$i] = collect([roundTenth($avgModulesSem1),roundTenth($avgModulesSem2)])->avg();
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = collect([roundTenth($avgGlobalCFCSem1),roundTenth($avgGlobalCFCSem2)])->avg();
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = collect([roundTenth($avPratSem1),roundTenth($avPratSem2)])->avg();
          }
          else {
            $indexsForYear++;// Go to second semester

            // Get grade semester 2 of year
            $semIndex = $gradesFormated['semesters']['countIndex'][$indexsForYear];
            $gradesFormated['otherAverages']['years']['modules'][$i] = $gradesFormated['otherAverages']['semesters']['modules'][$semIndex];
            $gradesFormated['categories']['Pratique']['avgYears'][$i] = $gradesFormated['categories']['Pratique']['avgSemesters'][$semIndex];
            $gradesFormated['otherAverages']['years']['globalCFC'][$i] = $gradesFormated['otherAverages']['semesters']['globalCFC'][$semIndex];
            $indexsForYear++;
          }
          break;

        default:
          abort(404);
          break;
      }

      $countOnlyLastSem = false;
    }

    // Set null if year not finished
    if (!$isYearFinished){
      $gradesFormated['otherAverages']['years']['modules'][$finishedYears + 1] = null;
      $gradesFormated['otherAverages']['years']['globalCFC'][$finishedYears + 1] = null;
      $gradesFormated['categories']['Pratique']['avgYears'][$finishedYears + 1] = null;

      switch ($this->class->formation->forName) {
        case 'MIN':
        case 'MID':
          $gradesFormated['categories']['Théorie MPT']['avgYears'][$finishedYears + 1] = null;
          break;

        case 'CIN':
        case 'CID':
          $gradesFormated['categories']['ECG']['avgYears'][$finishedYears + 1] = null;
          $gradesFormated['categories']['CBE']['avgYears'][$finishedYears + 1] = null;
          break;

        case 'FIN':
          // nothing special
          break;

        default:
          abort(404);
          break;
      }
    }

    // GLOBAL
    // Average Module I
    $iGrades = [];
    foreach ($gradesFormated['categories']['Modules I']['lessons'] as $lesson => $lessonDetails){
      $grades = [];
      foreach ($gradesFormated['semesters']['countIndex'] as $index){
        $grades[] = $lessonDetails['grades'][$index];
      }
      $avg = collect($grades)->avg();
      $gradesFormated['categories']['Modules I']['lessons'][$lesson]['avgGlobal'] = $avg;
      $iGrades[] = $avg;
    }
    $iAvg = collect($iGrades)->avg();
    $gradesFormated['categories']['Modules I']['avgGlobal'] = $iAvg;

    // Average Module C
    $cGrades = [];
    foreach ($gradesFormated['categories']['Modules C']['lessons'] as $lesson => $lessonDetails){
      $grades = [];
      foreach ($gradesFormated['semesters']['countIndex'] as $index){
        $grades[] = $lessonDetails['grades'][$index];
      }
      $avg = collect($grades)->avg();
      $gradesFormated['categories']['Modules C']['lessons'][$lesson]['avgGlobal'] = $avg;
      $cGrades[] = $avg;
    }
    $cAvg = collect($cGrades)->avg();
    $gradesFormated['categories']['Modules C']['avgGlobal'] = $cAvg;

    // Average Module
    if ($iAvg && $cAvg){
      $gradesFormated['otherAverages']['avgGlobal']['modules'] = roundTenth($iAvg)*0.8 + roundTenth($cAvg)*0.2;
    }
    else if ($iAvg){
      $gradesFormated['otherAverages']['avgGlobal']['modules'] = $iAvg;
    }
    else if ($cAvg){
      $gradesFormated['otherAverages']['avgGlobal']['modules'] = $cAvg;
    }
    else {
      $gradesFormated['otherAverages']['avgGlobal']['modules'] = null;
    }

    $gradesFormated['categories']['Pratique']['avgGlobal'] = null;// no need
    $gradesFormated['categories']['Pratique']['lessons']['PRAT']['avgGlobal'] = null;// no need
    $gradesFormated['otherAverages']['avgGlobal']['globalCFC'] = null;// no need
    switch ($this->class->formation->forName) {
      case 'MIN':
      case 'MID':
        $gradesFormated['categories']['TIB']['avgGlobal'] = null;
        $gradesFormated['categories']['TIB']['lessons']['TIB']['avgGlobal'] = null;

        $gradesFormated['categories']['TIP']['avgGlobal'] = null;
        $gradesFormated['categories']['TIP']['lessons']['TIP']['avgGlobal'] = null;

        // Average MPT's lessons
        $mptAvgs = [];
        foreach ($gradesFormated['categories']['Théorie MPT']['lessons'] as $lesson => $lessonDetails){
          $grades = [];
          foreach ($gradesFormated['semesters']['countIndex'] as $index){
            $grades[] = $lessonDetails['grades'][$index];
          }
          $avg = collect($grades)->avg();
          $gradesFormated['categories']['Théorie MPT']['lessons'][$lesson]['avgGlobal'] = $avg;
          $mptAvgs[] = roundFifth($avg);
        }

        // Average MPT
        $gradesFormated['categories']['Théorie MPT']['avgGlobal'] = collect($mptAvgs)->avg();

        // Average Science
        $gradesFormated['categories']['Sciences naturelles']['avgGlobal'] = $gradesFormated['categories']['Théorie MPT']['lessons']['SCINAT']['avgGlobal'];

        // lessons sciences
        foreach ($gradesFormated['categories']['Sciences naturelles']['lessons'] as $lesson => $lessonDetails){
          $grades = [];
          foreach ($gradesFormated['semesters']['countIndex'] as $index){
            $grades[] = $lessonDetails['grades'][$index];
          }
          $avg = collect($grades)->avg();
          $gradesFormated['categories']['Sciences naturelles']['lessons'][$lesson]['avgGlobal'] = $avg;
        }

        break;

      case 'CIN':
      case 'CID':
        // Average CBE's lessons
        $cbeAvgs = [];
        foreach ($gradesFormated['categories']['CBE']['lessons'] as $lesson => $lessonDetails){
          $grades = [];
          foreach ($gradesFormated['semesters']['countIndex'] as $index){
            $grades[] = $lessonDetails['grades'][$index];
          }
          $avg = collect($grades)->avg();
          $gradesFormated['categories']['CBE']['lessons'][$lesson]['avgGlobal'] = $avg;
          $cbeAvgs[] = roundFifth($avg);
        }

        // Average CBE
        $gradesFormated['categories']['CBE']['avgGlobal'] = collect($cbeAvgs)->avg();

        // Average ECG's lessons
        $ecgAvgs = [];
        foreach ($gradesFormated['categories']['ECG']['lessons'] as $lesson => $lessonDetails){
          $grades = [];
          foreach ($gradesFormated['semesters']['countIndex'] as $index){
            $grades[] = $lessonDetails['grades'][$index];
          }
          $avg = collect($grades)->avg();
          $gradesFormated['categories']['ECG']['lessons'][$lesson]['avgGlobal'] = $avg;
          $ecgAvgs[] = roundFifth($avg);
        }

        // Average ECG
        $gradesFormated['categories']['ECG']['avgGlobal'] = collect($ecgAvgs)->avg();
        break;

      case 'FIN':
        // Nothing to do..
        break;

      default:
        abort(404);
        break;
    }

    return $gradesFormated;
  }
}
