<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for lessons

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Formation;
use App\Models\Grade;

class Lesson extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idLesson', 'lesName', 'lesMergeableMpt', 'lesArchived'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_lesson';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idLesson';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The category to which the lesson belongs
   *
   * @return array Category
   */
  public function category() {
      return $this->belongsTo(Category::class,'fkCategory','idCategory')->orderBy('catName', 'asc');
  }

  /**
   * The formations that belong to the lesson.
   *
   * By default ordered by formation's name ascending
   *
   * @return array Formation
   */
  public function formations() {
      return $this->belongsToMany(Formation::class,'t_formation_lesson','fkLesson','fkFormation')->orderBy('forName','asc');
  }

  /**
   * The grades for the lesson
   *
   * By default ordered by semesters descending
   *
   * @return array Grade
   */
  public function grades(){
      return $this->hasMany(Grade::class,'fkLesson','idLesson')->orderBy('graSemester','desc');
  }
}
