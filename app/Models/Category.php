<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Model for categories

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lesson;

class Category extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idCategory', 'catName'
  ];

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 't_category';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'idCategory';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = true;

  /**
   * Indicates if the timestamps are saved.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The lessons that belong to the category.
   *
   * By default ordered by lesson's name ascending
   *
   * @return array Lesson
   */
  public function lessons() {
      return $this->hasMany(Lesson::class,'t_lesson','fkLesson','idLesson')->orderBy('lesName','asc');
  }
}
