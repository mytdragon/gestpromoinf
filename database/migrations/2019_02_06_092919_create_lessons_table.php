<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_lesson table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_lesson', function (Blueprint $table) {
            $table->increments('idLesson');
            $table->string('lesName', 50)->unique();
            $table->boolean('lesMergeableMpt')->default(false);
            $table->boolean('lesArchived')->default(false);
            $table->unsignedInteger('fkCategory');
            $table->foreign('fkCategory')->references('idCategory')->on('t_category')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_lesson');
    }
}
