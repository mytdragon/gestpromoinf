<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_teacher table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_teacher', function (Blueprint $table) {
            $table->increments('idTeacher');
            $table->string('teaUsername', 20)->nullable()->unique();
            $table->string('teaPassword', 60)->nullable();
            $table->text('teaFirstname');
            $table->text('teaLastname');
            $table->boolean('teaIsAdmin')->default(false);
            $table->boolean('teaArchived')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_teachers');
    }
}
