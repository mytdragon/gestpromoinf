<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 07.03.2019
/// Description: Creae t_student_promotion table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTStudentPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_student_promotion', function (Blueprint $table) {
          $table->increments('idStudentPromo');
          $table->unsignedInteger('fkStudent');
          $table->unsignedInteger('fkPromotion');
          $table->unsignedTinyInteger('sprSemester');
          $table->boolean('sprRepetition')->default(false);
          $table->foreign('fkStudent')->references('idStudent')->on('t_student')->onDelete('restrict')->onUpdate('cascade');
          $table->foreign('fkPromotion')->references('idPromotion')->on('t_promotion')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_student_promotion');
    }
}
