<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_category table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_category', function (Blueprint $table) {
            $table->increments('idCategory');
            $table->string('catName','50')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_category');
    }
}
