<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_class table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_class', function (Blueprint $table) {
            $table->increments('idClass');
            $table->string('claName',10)->unique();
            $table->boolean('claArchived')->default(false);
            $table->unsignedInteger('fkTeacher');
            $table->unsignedInteger('fkFormation');
            $table->foreign('fkTeacher')->references('idTeacher')->on('t_teacher')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('fkFormation')->references('idFormation')->on('t_formation')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
