<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_grade table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_grade', function (Blueprint $table) {
            $table->increments('idGrade');
            $table->float('graNumber',2,1);
            $table->unsignedTinyInteger('graSemester');
            $table->boolean('graRepetition')->default(false);
            $table->unsignedInteger('fkStudent');
            $table->unsignedInteger('fkLesson');
            $table->foreign('fkStudent')->references('idStudent')->on('t_student')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('fkLesson')->references('idLesson')->on('t_lesson')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_grade');
    }
}
