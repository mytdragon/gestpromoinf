<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 07.03.2019
/// Description: Creae t_promotion table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_promotion', function (Blueprint $table) {
            $table->increments('idPromotion');
            $table->string('proName', 20)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_promotion');
    }
}
