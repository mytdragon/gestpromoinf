<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_student table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_student', function (Blueprint $table) {
            $table->increments('idStudent');
            $table->text('stuFirstname');
            $table->text('stuLastname');
            $table->boolean('stuConditional')->default(false);
            $table->boolean('stuRepetition')->default(false);
            $table->boolean('stuArchived')->default(false);
            $table->unsignedInteger('fkClass');
            $table->foreign('fkClass')->references('idClass')->on('t_class')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_student');
    }
}
