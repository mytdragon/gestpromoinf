<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae t_formation_lesson table migration

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationLessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_formation_lesson', function (Blueprint $table) {
          $table->unsignedInteger('fkFormation');
          $table->unsignedInteger('fkLesson');
          $table->primary(['fkFormation', 'fkLesson']);
          $table->foreign('fkFormation')->references('idFormation')->on('t_formation')->onDelete('restrict')->onUpdate('cascade');
          $table->foreign('fkLesson')->references('idLesson')->on('t_lesson')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_formation_lesson');
    }
}
