<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae lesson seeder

use Illuminate\Database\Seeder;
use App\Models\Lesson;
use App\Models\Category;
use App\Models\Formation;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get formations
        $cin = Formation::where('forName', 'CIN')->first();
        $cid = Formation::where('forName', 'CID')->first();
        $min = Formation::where('forName', 'MIN')->first();
        $mid = Formation::where('forName', 'MID')->first();
        $fin = Formation::where('forName', 'FIN')->first();

        // Get categories
        $ecg = Category::where('catName', 'ECG')->first();
        $cbe = Category::where('catName', 'CBE')->first();
        $catMpt = Category::where('catName', 'Théorie MPT')->first();
        $catScinat = Category::where('catName', 'Sciences naturelles')->first();
        $catTib = Category::where('catName', 'TIB')->first();
        $catTip = Category::where('catName', 'TIP')->first();
        $modulei = Category::where('catName', 'Modules I')->first();
        $modulec = Category::where('catName', 'Modules C')->first();
        $project = Category::where('catName', 'Projets')->first();
        $pratique = Category::where('catName', 'Pratique')->first();

        // Create lessons
        $laco = new Lesson();
        $laco->lesName = 'LACO';
        $laco->lesMergeableMpt = 0;
        $laco->category()->associate($ecg);
        $laco->save();

        $soci = new Lesson();
        $soci->lesName = 'SOCI';
        $soci->lesMergeableMpt = 0;
        $soci->category()->associate($ecg);
        $soci->save();

        $mathcfc = new Lesson();
        $mathcfc->lesName = 'MATHCFC';
        $mathcfc->lesMergeableMpt = 0;
        $mathcfc->category()->associate($cbe);
        $mathcfc->save();

        $snat = new Lesson();
        $snat->lesName = 'SNAT';
        $snat->lesMergeableMpt = 0;
        $snat->category()->associate($cbe);
        $snat->save();

        $phys = new Lesson();
        $phys->lesName = 'PHYS';
        $phys->lesMergeableMpt = 0;
        $phys->category()->associate($catScinat);
        $phys->save();

        $chim = new Lesson();
        $chim->lesName = 'CHIM';
        $chim->lesMergeableMpt = 0;
        $chim->category()->associate($catScinat);
        $chim->save();

        $ecoe = new Lesson();
        $ecoe->lesName = 'ECOE';
        $ecoe->lesMergeableMpt = 0;
        $ecoe->category()->associate($cbe);
        $ecoe->save();

        $angt = new Lesson();
        $angt->lesName = 'ANGT';
        $angt->lesMergeableMpt = 0;
        $angt->category()->associate($cbe);
        $angt->save();

        $fran = new Lesson();
        $fran->lesName = 'FRAN';
        $fran->lesMergeableMpt = 0;
        $fran->category()->associate($catMpt);
        $fran->save();

        $alle = new Lesson();
        $alle->lesName = 'ALLE';
        $alle->lesMergeableMpt = 0;
        $alle->category()->associate($catMpt);
        $alle->save();

        $angl = new Lesson();
        $angl->lesName = 'ANGL';
        $angl->lesMergeableMpt = 1;
        $angl->category()->associate($catMpt);
        $angl->save();

        $mathfon = new Lesson();
        $mathfon->lesName = 'MATHFON';
        $mathfon->lesMergeableMpt = 1;
        $mathfon->category()->associate($catMpt);
        $mathfon->save();

        $mathspe = new Lesson();
        $mathspe->lesName = 'MATHSPE';
        $mathspe->lesMergeableMpt = 1;
        $mathspe->category()->associate($catMpt);
        $mathspe->save();

        $math = new Lesson();
        $math->lesName = 'MATH';
        $math->lesMergeableMpt = 1;
        $math->category()->associate($catMpt);
        $math->save();

        $scinat = new Lesson();
        $scinat->lesName = 'SCINAT';
        $scinat->lesMergeableMpt = 1;
        $scinat->category()->associate($catMpt);
        $scinat->save();

        $hispol = new Lesson();
        $hispol->lesName = 'HISPOL';
        $hispol->lesMergeableMpt = 0;
        $hispol->category()->associate($catMpt);
        $hispol->save();

        $ecdr = new Lesson();
        $ecdr->lesName = 'ECDR';
        $ecdr->lesMergeableMpt = 1;
        $ecdr->category()->associate($catMpt);
        $ecdr->save();

        $tib = new Lesson();
        $tib->lesName = 'TIB';
        $tib->lesMergeableMpt = 0;
        $tib->category()->associate($catTib);
        $tib->save();

        $tip = new Lesson();
        $tip->lesName = 'TIP';
        $tip->lesMergeableMpt = 0;
        $tip->category()->associate($catTip);
        $tip->save();

        $i100 = new Lesson();
        $i100->lesName = 'I100';
        $i100->lesMergeableMpt = 0;
        $i100->category()->associate($modulei);
        $i100->save();

        $i104 = new Lesson();
        $i104->lesName = 'I104';
        $i104->lesMergeableMpt = 0;
        $i104->category()->associate($modulei);
        $i104->save();

        $i114 = new Lesson();
        $i114->lesName = 'I114';
        $i114->lesMergeableMpt = 0;
        $i114->category()->associate($modulei);
        $i114->save();

        $i117 = new Lesson();
        $i117->lesName = 'I117';
        $i117->lesMergeableMpt = 0;
        $i117->category()->associate($modulei);
        $i117->save();

        $i120 = new Lesson();
        $i120->lesName = 'I120';
        $i120->lesMergeableMpt = 0;
        $i120->category()->associate($modulei);
        $i120->save();

        $i122 = new Lesson();
        $i122->lesName = 'I122';
        $i122->lesMergeableMpt = 0;
        $i122->category()->associate($modulei);
        $i122->save();

        $i123 = new Lesson();
        $i123->lesName = 'I123';
        $i123->lesMergeableMpt = 0;
        $i123->category()->associate($modulei);
        $i123->save();

        $i126 = new Lesson();
        $i126->lesName = 'I126';
        $i126->lesMergeableMpt = 0;
        $i126->category()->associate($modulei);
        $i126->save();

        $i129 = new Lesson();
        $i129->lesName = 'I129';
        $i129->lesMergeableMpt = 0;
        $i129->category()->associate($modulei);
        $i129->save();

        $i133 = new Lesson();
        $i133->lesName = 'I133';
        $i133->lesMergeableMpt = 0;
        $i133->category()->associate($modulei);
        $i133->save();

        $i143 = new Lesson();
        $i143->lesName = 'I143';
        $i143->lesMergeableMpt = 0;
        $i143->category()->associate($modulei);
        $i143->save();

        $i146 = new Lesson();
        $i146->lesName = 'I146';
        $i146->lesMergeableMpt = 0;
        $i146->category()->associate($modulei);
        $i146->save();

        $i150 = new Lesson();
        $i150->lesName = 'I150';
        $i150->lesMergeableMpt = 0;
        $i150->category()->associate($modulei);
        $i150->save();

        $i151 = new Lesson();
        $i151->lesName = 'I151';
        $i151->lesMergeableMpt = 0;
        $i151->category()->associate($modulei);
        $i151->save();

        $i152 = new Lesson();
        $i152->lesName = 'I152';
        $i152->lesMergeableMpt = 0;
        $i152->category()->associate($modulei);
        $i152->save();

        $i153 = new Lesson();
        $i153->lesName = 'I153';
        $i153->lesMergeableMpt = 0;
        $i153->category()->associate($modulei);
        $i153->save();

        $i155 = new Lesson();
        $i155->lesName = 'I155';
        $i155->lesMergeableMpt = 0;
        $i155->category()->associate($modulei);
        $i155->save();

        $i158 = new Lesson();
        $i158->lesName = 'I158';
        $i158->lesMergeableMpt = 0;
        $i158->category()->associate($modulei);
        $i158->save();

        $i159 = new Lesson();
        $i159->lesName = 'I159';
        $i159->lesMergeableMpt = 0;
        $i159->category()->associate($modulei);
        $i159->save();

        $i182 = new Lesson();
        $i182->lesName = 'I182';
        $i182->lesMergeableMpt = 0;
        $i182->category()->associate($modulei);
        $i182->save();

        $i183 = new Lesson();
        $i183->lesName = 'I183';
        $i183->lesMergeableMpt = 0;
        $i183->category()->associate($modulei);
        $i183->save();

        $i214 = new Lesson();
        $i214->lesName = 'I214';
        $i214->lesMergeableMpt = 0;
        $i214->category()->associate($modulei);
        $i214->save();

        $i226a = new Lesson();
        $i226a->lesName = 'I226A';
        $i226a->lesMergeableMpt = 0;
        $i226a->category()->associate($modulei);
        $i226a->save();

        $i226b = new Lesson();
        $i226b->lesName = 'I226B';
        $i226b->lesMergeableMpt = 0;
        $i226b->category()->associate($modulei);
        $i226b->save();

        $i239 = new Lesson();
        $i239->lesName = 'I239';
        $i239->lesMergeableMpt = 0;
        $i239->category()->associate($modulei);
        $i239->save();

        $i242 = new Lesson();
        $i242->lesName = 'I242';
        $i242->lesMergeableMpt = 0;
        $i242->category()->associate($modulei);
        $i242->save();

        $i306 = new Lesson();
        $i306->lesName = 'I306';
        $i306->lesMergeableMpt = 0;
        $i306->category()->associate($modulei);
        $i306->save();

        $i326 = new Lesson();
        $i326->lesName = 'I326';
        $i326->lesMergeableMpt = 0;
        $i326->category()->associate($modulei);
        $i326->save();

        $i403 = new Lesson();
        $i403->lesName = 'I403';
        $i403->lesMergeableMpt = 0;
        $i403->category()->associate($modulei);
        $i403->save();

        $i404 = new Lesson();
        $i404->lesName = 'I404';
        $i404->lesMergeableMpt = 0;
        $i404->category()->associate($modulei);
        $i404->save();

        $i411 = new Lesson();
        $i411->lesName = 'I411';
        $i411->lesMergeableMpt = 0;
        $i411->category()->associate($modulei);
        $i411->save();

        $i426 = new Lesson();
        $i426->lesName = 'I426';
        $i426->lesMergeableMpt = 0;
        $i426->category()->associate($modulei);
        $i426->save();

        $i431 = new Lesson();
        $i431->lesName = 'I431';
        $i431->lesMergeableMpt = 0;
        $i431->category()->associate($modulei);
        $i431->save();

        $i437 = new Lesson();
        $i437->lesName = 'I437';
        $i437->lesMergeableMpt = 0;
        $i437->category()->associate($modulei);
        $i437->save();

        $c101 = new Lesson();
        $c101->lesName = 'C101';
        $c101->lesMergeableMpt = 0;
        $c101->category()->associate($modulec);
        $c101->save();

        $c105 = new Lesson();
        $c105->lesName = 'C105';
        $c105->lesMergeableMpt = 0;
        $c105->category()->associate($modulec);
        $c105->save();

        $c127 = new Lesson();
        $c127->lesName = 'C127';
        $c127->lesMergeableMpt = 0;
        $c127->category()->associate($modulec);
        $c127->save();

        $c302 = new Lesson();
        $c302->lesName = 'C302';
        $c302->lesMergeableMpt = 0;
        $c302->category()->associate($modulec);
        $c302->save();

        $c304 = new Lesson();
        $c304->lesName = 'C304';
        $c304->lesMergeableMpt = 0;
        $c304->category()->associate($modulec);
        $c304->save();

        $c305 = new Lesson();
        $c305->lesName = 'C305';
        $c305->lesMergeableMpt = 0;
        $c305->category()->associate($modulec);
        $c305->save();

        $c318 = new Lesson();
        $c318->lesName = 'C318';
        $c318->lesMergeableMpt = 0;
        $c318->category()->associate($modulec);
        $c318->save();

        $c335 = new Lesson();
        $c335->lesName = 'C335';
        $c335->lesMergeableMpt = 0;
        $c335->category()->associate($modulec);
        $c335->save();

        $c340 = new Lesson();
        $c340->lesName = 'C340';
        $c340->lesMergeableMpt = 0;
        $c340->category()->associate($modulec);
        $c340->save();

        $p10 = new Lesson();
        $p10->lesName = '010-P_Eleoc';
        $p10->lesMergeableMpt = 0;
        $p10->category()->associate($project);
        $p10->save();

        $p11 = new Lesson();
        $p11->lesName = '011-P_Linux';
        $p11->lesMergeableMpt = 0;
        $p11->category()->associate($project);
        $p11->save();

        $p12 = new Lesson();
        $p12->lesName = '012-P_Bureautique';
        $p12->lesMergeableMpt = 0;
        $p12->category()->associate($project);
        $p12->save();

        $p13 = new Lesson();
        $p13->lesName = '013-P_WebStat';
        $p13->lesMergeableMpt = 0;
        $p13->category()->associate($project);
        $p13->save();

        $p14 = new Lesson();
        $p14->lesName = '014-P_GesProj';
        $p14->lesMergeableMpt = 0;
        $p14->category()->associate($project);
        $p14->save();

        $p15 = new Lesson();
        $p15->lesName = '015-P_Analyse';
        $p15->lesMergeableMpt = 0;
        $p15->category()->associate($project);
        $p15->save();

        $p20 = new Lesson();
        $p20->lesName = '020-P_Fab';
        $p20->lesMergeableMpt = 0;
        $p20->category()->associate($project);
        $p20->save();

        $p21 = new Lesson();
        $p21->lesName = '021-P_PME';
        $p21->lesMergeableMpt = 0;
        $p21->category()->associate($project);
        $p21->save();

        $p22 = new Lesson();
        $p22->lesName = '022-P_Prog';
        $p22->lesMergeableMpt = 0;
        $p22->category()->associate($project);
        $p22->save();

        $p23 = new Lesson();
        $p23->lesName = '023-P_VBA';
        $p23->lesMergeableMpt = 0;
        $p23->category()->associate($project);
        $p23->save();

        $p24 = new Lesson();
        $p24->lesName = '024-P_DeoSecu';
        $p24->lesMergeableMpt = 0;
        $p24->category()->associate($project);
        $p24->save();

        $p25 = new Lesson();
        $p25->lesName = '025-P_SecApp';
        $p25->lesMergeableMpt = 0;
        $p25->category()->associate($project);
        $p25->save();

        $p30 = new Lesson();
        $p30->lesName = '030-P_Formation';
        $p30->lesMergeableMpt = 0;
        $p30->category()->associate($project);
        $p30->save();

        $p31 = new Lesson();
        $p31->lesName = '031-P_Reseau';
        $p31->lesMergeableMpt = 0;
        $p31->category()->associate($project);
        $p31->save();

        $p32 = new Lesson();
        $p32->lesName = '032-P_Dev';
        $p32->lesMergeableMpt = 0;
        $p32->category()->associate($project);
        $p32->save();

        $p33 = new Lesson();
        $p33->lesName = '033-P_Mobile';
        $p33->lesMergeableMpt = 0;
        $p33->category()->associate($project);
        $p33->save();

        $p34 = new Lesson();
        $p34->lesName = '034-P_Algo';
        $p34->lesMergeableMpt = 0;
        $p34->category()->associate($project);
        $p34->save();

        $p40 = new Lesson();
        $p40->lesName = '040-P_Web2';
        $p40->lesMergeableMpt = 0;
        $p40->category()->associate($project);
        $p40->save();

        $p41 = new Lesson();
        $p41->lesName = '041-P_Script';
        $p41->lesMergeableMpt = 0;
        $p41->category()->associate($project);
        $p41->save();

        $p42 = new Lesson();
        $p42->lesName = '042-P_GesProj2';
        $p42->lesMergeableMpt = 0;
        $p42->category()->associate($project);
        $p42->save();

        $p43 = new Lesson();
        $p43->lesName = '043-P_Infra';
        $p43->lesMergeableMpt = 0;
        $p43->category()->associate($project);
        $p43->save();

        $p44 = new Lesson();
        $p44->lesName = '044-P_OO';
        $p44->lesMergeableMpt = 0;
        $p44->category()->associate($project);
        $p44->save();

        $p50 = new Lesson();
        $p50->lesName = '050-P_VM';
        $p50->lesMergeableMpt = 0;
        $p50->category()->associate($project);
        $p50->save();

        $p51 = new Lesson();
        $p51->lesName = '051-P_AdminSys';
        $p51->lesMergeableMpt = 0;
        $p51->category()->associate($project);
        $p51->save();

        $p52 = new Lesson();
        $p52->lesName = '052-P_SrvWeb';
        $p52->lesMergeableMpt = 0;
        $p52->category()->associate($project);
        $p52->save();

        $p53 = new Lesson();
        $p53->lesName = '053-P_3eme';
        $p53->lesMergeableMpt = 0;
        $p53->category()->associate($project);
        $p53->save();

        $p54 = new Lesson();
        $p54->lesName = '054-P_Support';
        $p54->lesMergeableMpt = 0;
        $p54->category()->associate($project);
        $p54->save();

        $p55 = new Lesson();
        $p55->lesName = '055-P_WebDyn3';
        $p55->lesMergeableMpt = 0;
        $p55->category()->associate($project);
        $p55->save();

        $p56 = new Lesson();
        $p56->lesName = '056-P_ModData';
        $p56->lesMergeableMpt = 0;
        $p56->category()->associate($project);
        $p56->save();

        $p57 = new Lesson();
        $p57->lesName = '057-P_MobApp';
        $p57->lesMergeableMpt = 0;
        $p57->category()->associate($project);
        $p57->save();

        $p80 = new Lesson();
        $p80->lesName = '080-P_4eme';
        $p80->lesMergeableMpt = 0;
        $p80->category()->associate($project);
        $p80->save();

        $pGlobal = new Lesson();
        $pGlobal->lesName = 'PRAT';
        $pGlobal->lesMergeableMpt = 0;
        $pGlobal->category()->associate($pratique);
        $pGlobal->save();

        // Link lessons with formations
        $cin->lessons()->saveMany([
          $laco, $soci, $mathcfc, $snat, $ecoe, $angt,
          $i100, $i104, $i114, $i117, $i120, $i122, $i123, $i126, $i129, $i133, $i143, $i146, $i151, $i158, $i159, $i182, $i214, $i226a, $i226b, $i239, $i306, $i326, $i403, $i404, $i411, $i426, $i431, $i437,
          $c101, $c105, $c127, $c302, $c304, $c305, $c318, $c340,
          $pGlobal
        ]);

        $cid->lessons()->saveMany([
          $laco, $soci, $mathcfc, $snat, $ecoe, $angt,
          $i100, $i104, $i114, $i117, $i120, $i122, $i123, $i133, $i150, $i151, $i152, $i153, $i155, $i183, $i214, $i226a, $i226b, $i242, $i306, $i326, $i403, $i404, $i411, $i426, $i431,
          $c101, $c105, $c302, $c304, $c305, $c318, $c335,
          $pGlobal
        ]);

        $min->lessons()->saveMany([
          $fran, $alle, $angl, $mathfon, $mathspe, $math, $scinat, $hispol, $ecdr, $tib, $tip, $chim, $phys,
          $i100, $i104, $i114, $i117, $i120, $i122, $i123, $i126, $i129, $i133, $i143, $i146, $i151, $i158, $i159, $i182, $i214, $i226a, $i226b, $i239, $i306, $i326, $i403, $i404, $i411, $i426, $i431, $i437,
          $c101, $c105, $c127, $c302, $c304, $c305, $c318, $c340,
          $pGlobal
        ]);

        $mid->lessons()->saveMany([
          $fran, $alle, $angl, $mathfon, $mathspe, $math, $scinat, $hispol, $ecdr, $tib, $tip, $chim, $phys,
          $i100, $i104, $i114, $i117, $i120, $i122, $i123, $i133, $i150, $i151, $i152, $i153, $i155, $i183, $i214, $i226a, $i226b, $i242, $i306, $i326, $i403, $i404, $i411, $i426, $i431,
          $c101, $c105, $c302, $c304, $c305, $c318, $c335,
          $pGlobal
        ]);

        $fin->lessons()->saveMany([
          $i100, $i104, $i114, $i117, $i120, $i122, $i123, $i126, $i129, $i133, $i143, $i146, $i151, $i158, $i159, $i182, $i214, $i226a, $i226b, $i239, $i306, $i326, $i403, $i404, $i411, $i426, $i431, $i437,
          $c101, $c105, $c127, $c302, $c304, $c305, $c318, $c340,
          $pGlobal
        ]);
    }
}
