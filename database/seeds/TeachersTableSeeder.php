<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae teacher seeder

use Illuminate\Database\Seeder;
use App\Models\Teacher;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $machin = Teacher::create([
          'teaUsername' => 'admin',
          'teaPassword' => '$2y$12$oIk7TrfuTHNRJuFpKZkEEu9myDlCBNwwRWMpAO2bZiz07oEM45cwy',
          'teaAcronym' => 'ADMIN',
          'teaFirstname' => 'admin',
          'teaLastname' => 'local',
          'teaIsAdmin' => true
        ]);
    }
}
