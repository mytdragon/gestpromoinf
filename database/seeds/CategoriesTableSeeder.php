<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae category seeder

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['catName' => 'ECG']);
        Category::create(['catName' => 'CBE']);
        Category::create(['catName' => 'Théorie MPT']);
        Category::create(['catName' => 'Sciences naturelles']);
        Category::create(['catName' => 'Modules I']);
        Category::create(['catName' => 'Modules C']);
        Category::create(['catName' => 'Pratique']);
        Category::create(['catName' => 'Projets']);
        Category::create(['catName' => 'TIB']);
        Category::create(['catName' => 'TIP']);
    }
}
