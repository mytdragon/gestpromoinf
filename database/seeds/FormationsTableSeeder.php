<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae formation seeder

use Illuminate\Database\Seeder;
use App\Models\Formation;

class FormationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Formation::create(['forName' => 'CIN', 'forYears' => 4]);
      Formation::create(['forName' => 'CID', 'forYears' => 4]);
      Formation::create(['forName' => 'MIN', 'forYears' => 4]);
      Formation::create(['forName' => 'MID', 'forYears' => 4]);
      Formation::create(['forName' => 'FIN', 'forYears' => 1]);
    }
}
