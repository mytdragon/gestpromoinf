<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.02.2019
/// Description: Creae promotion seeder

use Illuminate\Database\Seeder;
use App\Models\Promotion;

class PromotionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::create(['proName' => 'Succès']);
        Promotion::create(['proName' => 'Conditionnel']);
        Promotion::create(['proName' => 'Echec']);
        Promotion::create(['proName' => 'Extraordinaire']);
    }
}
