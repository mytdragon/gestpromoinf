<?php

return [
  'sheetName' => 'Semestre',
  'class' => 'A2',
  'teacher' => 'D3', // NOTE how do if it's for exemple 'AGX' instead 'firstname lastname'

  // used to find the number of student
  'firstStudentRow' => '5',
  'columnToStopStudent' => 'C',// The column that will browse untill it found the text that means there is no more student
  'textForStopStudent' => 'Moyenne de classe',// Text that means there is no more student

  'lessonsRow' => '4',

  'students' => [
    'no' => 'A',
    'firstname' => 'B',
    'lastname' => 'C',
  ],
];
