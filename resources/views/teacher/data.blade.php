@foreach ($teachers as $teacher)
  <tr>
    <th scope="row">{{$teacher->idTeacher}}</th>
    <td>{{$teacher->teaAcronym}}</td>
    <td>{{$teacher->teaFirstname}}</td>
    <td>{{$teacher->teaLastname}}</td>
    <td>{{$teacher->classes()->pluck('claName')->implode('/')}}</td>
    <td align="center">
      @if ($teacher->teaArchived)
        <span class="glyphicon glyphicon-ok"></span>
      @else
        <span class="glyphicon glyphicon-remove cross"></span>
      @endif
    </td>
    <td class="actions">
      @if ($teacher->teaArchived)
        <a href="{{route('unarchiveTeacher', ['id' => $teacher->idTeacher])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="désarchiver"></a>
      @else
        <a href="{{route('archiveTeacher', ['id' => $teacher->idTeacher])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="archiver"></a>
      @endif
      <a href="{{route('editTeacher', ['id' => $teacher->idTeacher])}}"><span class="glyphicon glyphicon-edit"></span></a>
    </td>
  </tr>
@endforeach
