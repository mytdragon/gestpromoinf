@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    Modifier {{ $teacher->teaFirstname.' '.$teacher->teaLastname }}
  @elseif ($action == 'add')
    Ajouter un enseignant
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">
    @if ($action == 'edit')
      <h2>{{ $teacher->teaFirstname.' '.$teacher->teaLastname }}</h2>
    @elseif ($action == 'add')
      <h2>Ajouter un enseignant</h2>
    @endif
  </h1>

  @include('includes.session-message')

  <form method="POST" enctype="multipart/form-data"
  @if ($action == 'edit')
    action="{{ route('editTeacher', ['id' => $teacher->idTeacher]) }}"
  @elseif ($action == 'add')
    action="{{ route('addTeacher') }}"
  @endif
  >
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('acronym') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Acronyme</span>
          </div>
          <input type="text" class="form-control" name="acronym" value="{{isset($teacher) ? $teacher->teaAcronym : old('acronym') }}" required>
        </div>


        @if ($errors->has('acronym'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('acronym') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Prénom</span>
          </div>
          <input type="text" class="form-control" name="firstname" value="{{isset($teacher) ? $teacher->teaFirstname : old('firstname') }}" required>
        </div>


        @if ($errors->has('firstname'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('firstname') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Nom</span>
          </div>
          <input type="text" class="form-control" name="lastname" value="{{isset($teacher) ? $teacher->teaLastname : old('lastname') }}" required>
        </div>


        @if ($errors->has('lastname'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('lastname') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col">
          <button type="submit" class="btn btn-primary">
            @if ($action == 'edit')
              Modifier
            @elseif ($action == 'add')
              Ajouter
            @endif
          </button>
      </div>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
