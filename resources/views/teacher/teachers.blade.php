@extends('layouts.app')

@section('title')
    Enseignants
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">Enseignants</h1>

  @include('includes.session-message')
  @include('includes.search-form', ['route' => 'teachers'])

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Acronyme</th>
        <th scope="col">Prénom</th>
        <th scope="col">Nom</th>
        <th scope="col">Classes</th>
        <th scope="col" class="text-center">Archivé</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('addTeacher')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="teachers-container">
        @include('teacher.data')
    </tbody>
  </table>

  @push('js')

  @endpush
@endsection
