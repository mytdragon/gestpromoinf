@if (Session::has('message'))
  @if (Session::get('message')['result'])
    <div class="alert alert-success" role="alert">
      <strong>{{ Session::get('message')['title'] }}</strong> {{ Session::get('message')['msg'] }}
    </div>
  @else
    <div class="alert alert-danger" role="alert">
      <strong>{{ Session::get('message')['title'] }}</strong> {{ Session::get('message')['msg'] }}
    </div>
  @endif
@endif
