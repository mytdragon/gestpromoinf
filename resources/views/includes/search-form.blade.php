<form id="search-form" method="GET" enctype="multipart/form-data" action="{{route($route)}}">
  <div class="input-group mb-3">
    <input id="searchbar-textbox" type="text" name="search" class="form-control" placeholder="Recherche.." value="{{Request::get('search')}}">
    <div class="input-group-append">
      <button id="reset-searchbar-button" class="btn btn-outline-secondary" type="button">Reset</button>
      <button id="submit-searchbar-button" class="btn btn-outline-secondary" type="submit">Rechercher</button>
    </div>
  </div>
</form>

@push('js')
  <script type="text/javascript">
    $('body').on('click', "#reset-searchbar-button", function () {
      $('#searchbar-textbox').val("");
      $("#submit-searchbar-button").trigger('click');
    });
  </script>
@endpush
