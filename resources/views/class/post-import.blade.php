@extends('layouts.app')

@section('title')
    Classes
@endsection

@push('css')

@endpush

@section('content')

  @include('includes.session-message')

  @if (isset($conflicts['student']))
    <div id="conflicts">
      <h1 class="mt-4">Conflits</h1>

      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Elève</th>
            <th scope="col">Cours</th>
            <th scope="col" class="text-center">Ancienne</th>
            <th scope="col" class="text-center">Nouvelle</th>
            <th class="actions" scope="col">Actions</th>
          </tr>
        </thead>
        <tbody id="conflicts-container">
          @foreach ($conflicts['student'] as $idStudent => $student)
            <tr id="group-student-{{$idStudent}}" data-student="{{$idStudent}}">
              <th colspan="4" data-toggle="collapse" data-target=".student-{{$idStudent}}">
                <span class="glyphicon glyphicon-triangle-bottom"></span>
                {{$student['student']['firstname']}} {{$student['student']['lastname']}}
              </th>
              <th class="text-right">
                <a href="#" class="grade-keep-all">Conserver</a> | <a href="{{route('replaceGrade')}}" class="grade-replace-all">Ecraser</a> | <a href="{{route('repeatGrade')}}" class="grade-repeat-all">Redoublement</a>
              </th>
            </tr>

            @foreach ($student['conflicts'] as $conflict)
              <tr class="collapse student-{{$idStudent}}" data-student="{{$idStudent}}" data-semester="{{$conflict['semester']}}" data-grade="{{$conflict['grade']['id']}}" data-new="{{$conflict['grade']['newGrade']}}">
                <td>{{$student['student']['firstname']}} {{$student['student']['lastname']}}</td>
                <td>{{$conflict['grade']['lesson']}}</td>
                <td align="center">{{$conflict['grade']['number']}}</td>
                <td align="center">{{$conflict['grade']['newGrade']}}</td>
                <td align="right">
                  <a href="#" class="grade-keep">Conserver</a> | <a href="{{route('replaceGrade')}}" class="grade-replace">Ecraser</a> | <a href="{{route('repeatGrade')}}" class="grade-repeat">Redoublement</a>
                </td>
              </tr>
            @endforeach
          @endforeach
        </tbody>
      </table>
    </div>
  @endif

  @if (isset($conflicts['teacher']))
  <div id="teacher">
    <h1 class="mt-4">Enseignant</h1>

    <form id="personal-informations-teacher-form" method="POST" enctype="multipart/form-data" action="{{ route('editTeacher', ['id' => $conflicts['teacher']]) }}">
      {{ csrf_field() }}

      <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
        <div class="col">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Prénom</span>
            </div>
            <input type="text" class="form-control" name="firstname" value="" required>
          </div>
        </div>
      </div>

      <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
        <div class="col">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Nom</span>
            </div>
            <input type="text" class="form-control" name="lastname" value="" required>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col">
            <button type="submit" class="btn btn-primary">Sauvegarder</button>
        </div>
      </div>

    </form>
  </div>
  @endif

  <input type="hidden" name="redirect" value="{{$route}}">  

  @push('js')
    <script src="{{ asset('js/import.js?v='.config('app.version')) }}"></script>
  @endpush
@endsection
