@extends('layouts.app')

@section('title')
    Classes
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">Classes</h1>

  @include('includes.session-message')

  <div class="mb-2">
    <form id="import-excel-file" action="{{route('import')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="file">
          <label class="custom-file-label" for="inputGroupFile04">Choisir un fichier .xlsx ou .xlsm</label>
        </div>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit">Importer</button>
        </div>
      </div>

      <input type="hidden" name="teacherFirstname" value="">
      <input type="hidden" name="teacherLastname" value="">
      <input type="hidden" name="semester" value="">
    </form>
  </div>

  @include('includes.search-form', ['route' => 'classes'])

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col">Enseignant</th>
        <th scope="col" class="text-center">Archivé</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('addClass')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="lessons-container">
        @include('class.data')
    </tbody>
  </table>


  @push('js')
    <script src="{{ asset('js/import.js?v='.config('app.version')) }}"></script>
  @endpush
@endsection
