<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/glyphicons.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-select.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/app.css?v='.config('app.version')) }}" rel="stylesheet">
</head>

<body>
  <h1 class="mt-4">{{$class->claName}}</h1>

  @for ($i = 0; $i <= $maxIndex; $i++)
    <div class="{{$i != $maxIndex ? 'page' : ''}}">
      <table id="grades-table" class="table table-sm table-bordered table-hover">
        <thead class="thead-light">
          <tr>
            <th scope="col">Semestre {{$i + 1}} - {{$class->claName}}</th>
            @foreach($students as $id => $student)
              <th scope="col" class="text-center"><a href="{{route('viewStudent', ['id' => $student['id']])}}">{{$student['name']}}</a></th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @foreach ($lessonsByCategory as $category => $lessons)
            <tr class="tr-borderless">
              <th>
                {{$category}}
              </th>

              @for ($j = 0; $j < count($class->students); $j++)
                <th scope="col" class="text-center">{{isset($students[$j]['followup']['semesters']['countIndex'][$i]) && isset($students[$j]['followup']['categories'][$category]['avgSemesters']) ? roundTenth($students[$j]['followup']['categories'][$category]['avgSemesters'][$students[$j]['followup']['semesters']['countIndex'][$i]]) : ''}}</th>
              @endfor
            </tr>

            @foreach ($lessons as $id => $lesson)
              <tr>
                <td class="lesson" scope="col">{{$lesson}}</td>
                @for ($j = 0; $j < count($students); $j++)
                  <td class="text-center">
                    {{isset($students[$j]['followup']['semesters']['countIndex'][$i]) ? $students[$j]['followup']['categories'][$category]['lessons'][$lesson]['grades'][$students[$j]['followup']['semesters']['countIndex'][$i]] : ''}}
                  </td>
                @endfor
              </tr>
            @endforeach
          @endforeach

          <!-- Modules -->
          <tr>
            <th>Modules (école et CIE)</th>
            @for ($j = 0; $j < count($students); $j++)
              <th scope="col" class="text-center">
                {{isset($students[$j]['followup']['semesters']['countIndex'][$i]) ? roundTenth($students[$j]['followup']['otherAverages']['semesters']['modules'][$students[$j]['followup']['semesters']['countIndex'][$i]]) : ''}}
              </th>
            @endfor
          </tr>

          <!-- Global CFC -->
          <tr>
            <th>Global CFC</th>
            @for ($j = 0; $j < count($students); $j++)
              <th scope="col" class="text-center">
                {{isset($students[$j]['followup']['semesters']['countIndex'][$i]) ? roundTenth($students[$j]['followup']['otherAverages']['semesters']['globalCFC'][$students[$j]['followup']['semesters']['countIndex'][$i]]) : '' }}
              </th>
            @endfor
          </tr>

          <!-- Promotion -->
          <tr>
            <th>Promotion</th>
            @for ($j = 0; $j < count($class->students); $j++)
              <th scope="col" class="text-center">
                @if (isset($students[$j]['followup']['promotions'][$i]))
                  {{isset($students[$j]['followup']['promotions'][$i.'\'']) ? $students[$j]['followup']['promotions'][$i.'\'']['name'] : $students[$j]['followup']['promotions'][$i]['name']}}
                @endif
              </th>
            @endfor
          </tr>
        </tbody>
      </table>
    </div>
  @endfor

</body>
</html>
