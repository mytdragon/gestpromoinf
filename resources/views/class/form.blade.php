@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    Modifier {{ $class->claName }}
  @elseif ($action == 'add')
    Ajouter une classe
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">
    @if ($action == 'edit')
      <h2>{{ $class->claName }}</h2>
    @elseif ($action == 'add')
      <h2>Ajouter une classe</h2>
    @endif
  </h1>

  @include('includes.session-message')

  <form method="POST" enctype="multipart/form-data"
  @if ($action == 'edit')
    action="{{ route('editClass', ['id' => $class->idClass]) }}"
  @elseif ($action == 'add')
    action="{{ route('addClass') }}"
  @endif
  >
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Nom</span>
          </div>
          <input type="text" class="form-control" name="name" value="{{isset($class) ? $class->claName : old('name') }}" required>
        </div>


        @if ($errors->has('name'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('name') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('formation') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Formation</label>
          </div>
          <select class="bootstrap-select" name="formation" data-live-search="true" data-width="1%" required>
            <option disabled selected value> -- Choisissez la formation -- </option>
            @foreach ($formations as $formation)
              <option value="{{$formation->idFormation}}" {{((isset($class) && $class->formation == $formation ) || old('formation') == $formation->idFormation) ? 'selected' : ''}}>{{$formation->forName}}</option>
            @endforeach
          </select>
        </div>

          @if ($errors->has('formation'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('formation') }}
              </div>
          @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('teacher') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Enseignant</label>
          </div>
          <select class="bootstrap-select" name="teacher" data-live-search="true" data-width="1%" required>
            <option disabled selected value> -- Choisissez l'enseignant -- </option>
            @foreach ($teachers as $teacher)
              <option value="{{$teacher->idTeacher}}" {{((isset($class) && $class->teacher == $teacher ) || old('formation') == $teacher->idTeacher) ? 'selected' : ''}}>{{$teacher->teaFirstname.' '.$teacher->teaLastname}}</option>
            @endforeach
          </select>
        </div>

          @if ($errors->has('teacher'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('teacher') }}
              </div>
          @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col">
          <button type="submit" class="btn btn-primary">
            @if ($action == 'edit')
              Modifier
            @elseif ($action == 'add')
              Ajouter
            @endif
          </button>
      </div>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
