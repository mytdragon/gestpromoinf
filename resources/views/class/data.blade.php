@foreach ($classes as $class)
  <tr>
    <th scope="row">{{$class->idClass}}</th>
    <td>{{$class->claName}}</td>
    <td>[{{$class->teacher->teaAcronym}}] {{$class->teacher->teaFirstname.' '.$class->teacher->teaLastname}}</td>
    <td align="center">
      @if ($class->claArchived)
        <span class="glyphicon glyphicon-ok"></span>
      @else
        <span class="glyphicon glyphicon-remove cross"></span>
      @endif
    </td>
    <td class="actions">
      @if ($class->claArchived)
        <a href="{{route('unarchiveClass', ['id' => $class->idClass])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="désarchiver"></a>
      @else
        <a href="{{route('archiveClass', ['id' => $class->idClass])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="archiver"></a>
      @endif
      <a href="{{route('editClass', ['id' => $class->idClass])}}"><span class="glyphicon glyphicon-edit"></span></a>
      <a href="{{ route('viewClass', ['id' => $class->idClass]) }}"><span class="glyphicon glyphicon-education"></span></a>
    </td>
  </tr>
@endforeach
