<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/glyphicons.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-select.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/app.css?v='.config('app.version')) }}" rel="stylesheet">
</head>

<body>

  <h1 class="mt-4">{{$class->claName}}</h1>

  <table id="grades-table" class="table table-sm table-bordered table-hover">
    <thead class="thead-light">
      <tr>
        <th scope="col">Semestre {{$semester}} - {{$class->claName}}</th>
        @foreach($students as $id => $student)
          <th scope="col" class="text-center"><a href="{{route('viewStudent', ['id' => $student['id']])}}">{{$student['name']}}</a></th>
        @endforeach
      </tr>
    </thead>
    <tbody>
      @foreach ($lessonsByCategory as $category => $lessons)
        <tr class="tr-borderless">
          <th>
            {{$category}}
          </th>

          @for ($i = 0; $i < count($class->students); $i++)
            <th scope="col" class="text-center">{{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) && isset($students[$i]['followup']['categories'][$category]['avgSemesters']) ? roundTenth($students[$i]['followup']['categories'][$category]['avgSemesters'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : ''}}</th>
          @endfor
        </tr>

        @foreach ($lessons as $id => $lesson)
          <tr>
            <td class="lesson" scope="col">{{$lesson}}</td>
            @for ($i = 0; $i < count($students); $i++)
              <td class="text-center">
                {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? $students[$i]['followup']['categories'][$category]['lessons'][$lesson]['grades'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]] : ''}}
              </td>
            @endfor
          </tr>
        @endforeach
      @endforeach

      <!-- Modules -->
      <tr>
        <th>Modules (école et CIE)</th>
        @for ($i = 0; $i < count($students); $i++)
          <th scope="col" class="text-center">
            {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? roundTenth($students[$i]['followup']['otherAverages']['semesters']['modules'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : ''}}
          </th>
        @endfor
      </tr>

      <!-- Global CFC -->
      <tr>
        <th>Global CFC</th>
        @for ($i = 0; $i < count($students); $i++)
          <th scope="col" class="text-center">
            {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? roundTenth($students[$i]['followup']['otherAverages']['semesters']['globalCFC'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : '' }}
          </th>
        @endfor
      </tr>

      <!-- Promotion -->
      <tr>
        <th>Promotion</th>
        @for ($i = 0; $i < count($class->students); $i++)
          <th scope="col" class="text-center">
            @if (isset($students[$i]['followup']['promotions'][$semester]))
              {{isset($students[$i]['followup']['promotions'][$semester.'\'']) ? $students[$i]['followup']['promotions'][$semester.'\'']['name'] : $students[$i]['followup']['promotions'][$semester]['name']}}
            @endif
          </th>
        @endfor
      </tr>
    </tbody>
  </table>

</body>
</html>
