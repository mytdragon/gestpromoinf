@extends('layouts.app')

@section('title')
    {{$class->claName}} - Semestre {{$semester}}
@endsection

@push('css')

@endpush

@section('content')
  <h1 class="mt-4">{{$class->claName}} - Semestre {{$semester}}</h1>

  @include('includes.session-message')

  <div class="mb-2">
    <a href="{{route('downloadBySemesterPDFClass', ['id' => $class->idClass, 'semester' => $semester])}}"><button id="export-followup-pdf" type="button" class="btn btn-light"><span class="glyphicon glyphicon-download-alt"></span> Exporter le semestre</button></a>
    <a href="{{route('downloadByAllPDFClass', ['id' => $class->idClass])}}"><button id="export-followup-pdf" type="button" class="btn btn-light"><span class="glyphicon glyphicon-download-alt"></span> Exporter tous les semestres</button></a>
  </div>

  <table id="grades-table" class="table table-sm table-bordered table-hover">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        @foreach($students as $id => $student)
          <th scope="col" class="text-center"><a href="{{route('viewStudent', ['id' => $student['id']])}}">{{$student['name']}}</a></th>
        @endforeach
      </tr>
    </thead>
    <tbody>
      @foreach ($lessonsByCategory as $category => $lessons)
        <tr class="tr-borderless" data-toggle="collapse" data-target=".cat-{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}">
          <th>
            <span class="glyphicon glyphicon-triangle-bottom"></span>
            {{$category}}
          </th>

          @for ($i = 0; $i < count($class->students); $i++)
            <th scope="col" class="text-center">{{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) && isset($students[$i]['followup']['categories'][$category]['avgSemesters']) ? roundTenth($students[$i]['followup']['categories'][$category]['avgSemesters'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : ''}}</th>
          @endfor
        </tr>

        @foreach ($lessons as $id => $lesson)
          <tr class="collapse cat-{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}">
            <td class="lesson" scope="col">{{$lesson}}</td>
            @for ($i = 0; $i < count($students); $i++)
              <td scope="col" class="text-center">
                {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? $students[$i]['followup']['categories'][$category]['lessons'][$lesson]['grades'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]] : ''}}
              </td>
            @endfor
          </tr>
        @endforeach
      @endforeach

      <!-- Modules -->
      <tr>
        <th>Modules (école et CIE)</th>
        @for ($i = 0; $i < count($students); $i++)
          <th scope="col" class="text-center">
            {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? roundTenth($students[$i]['followup']['otherAverages']['semesters']['modules'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : ''}}
          </th>
        @endfor
      </tr>

      <!-- Global CFC -->
      <tr>
        <th>Global CFC</th>
        @for ($i = 0; $i < count($students); $i++)
          <th scope="col" class="text-center">
            {{isset($students[$i]['followup']['semesters']['countIndex'][$semester - 1]) ? roundTenth($students[$i]['followup']['otherAverages']['semesters']['globalCFC'][$students[$i]['followup']['semesters']['countIndex'][$semester - 1]]) : '' }}
          </th>
        @endfor
      </tr>

      <!-- Promotion TODO -->
      <tr>
        <th>Promotion</th>
        @for ($i = 0; $i < count($class->students); $i++)
          <th scope="col" class="text-center">
            @if (isset($students[$i]['followup']['promotions'][$semester]))
              {{isset($students[$i]['followup']['promotions'][$semester.'\'']) ? $students[$i]['followup']['promotions'][$semester.'\'']['name'] : $students[$i]['followup']['promotions'][$semester]['name']}}
            @endif
          </th>
        @endfor
      </tr>
    </tbody>
  </table>

  @for ($i = 1; $i <= $class->formation->forYears * 2; $i++)
    @if ($i == $semester)
      <a href="{{route('viewClass', ['id' => $class->idClass, 'semester' => $i])}}"><button type="button" class="btn btn-secondary">Semestre {{$i}}</button></a>
    @else
      <a href="{{route('viewClass', ['id' => $class->idClass, 'semester' => $i])}}"><button type="button" class="btn btn-light">Semestre {{$i}}</button></a>
    @endif
  @endfor

  @push('js')

  @endpush
@endsection
