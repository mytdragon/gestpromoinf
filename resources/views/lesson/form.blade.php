@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    Modifier {{ $lesson->lesName }}
  @elseif ($action == 'add')
    Ajouter un cours
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">
    @if ($action == 'edit')
      <h2>{{ $lesson->lesName }}</h2>
    @elseif ($action == 'add')
      <h2>Ajouter un cours</h2>
    @endif
  </h1>

  @include('includes.session-message')

  <form method="POST" enctype="multipart/form-data"
  @if ($action == 'edit')
    action="{{ route('editLesson', ['id' => $lesson->idLesson]) }}"
  @elseif ($action == 'add')
    action="{{ route('addLesson') }}"
  @endif
  >
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Nom</span>
          </div>
          <input type="text" class="form-control" name="name" value="{{isset($lesson) ? $lesson->lesName : old('name') }}" required>
        </div>


        @if ($errors->has('name'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('name') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Catégorie</label>
          </div>
          <select class="bootstrap-select" name="category" data-live-search="true" data-width="1%" required>
            <option disabled selected value> -- Choisissez la catégorie -- </option>
            @foreach ($categories as $category)
              <option value="{{$category->idCategory}}" {{((isset($lesson) && $lesson->category == $category ) || old('category') == $category->idCategory) ? 'selected' : ''}}>{{$category->catName}}</option>
            @endforeach
          </select>
        </div>

          @if ($errors->has('category'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('category') }}
              </div>
          @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('formations') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Formations</label>
          </div>
          <select size='5' multiple name="formations[]" class="two-column-ms">
            @foreach ($formations as $formation)
              @if ((isset($lesson) && $lesson->formations->contains($formation)) || (!empty(old('formations')) && in_array($formation->idFormation, old('formations'))) )
                <option selected value="{{$formation->idFormation}}">{{$formation->forName}}</option>
              @else
                <option value="{{$formation->idFormation}}">{{$formation->forName}}</option>
              @endif
            @endforeach
          </select>
        </div>

          @if ($errors->has('formations'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('formations') }}
              </div>
          @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col">
          <button type="submit" class="btn btn-primary">
            @if ($action == 'edit')
              Modifier
            @elseif ($action == 'add')
              Ajouter
            @endif
          </button>
      </div>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
