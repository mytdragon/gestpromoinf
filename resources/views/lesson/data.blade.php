@foreach ($lessons as $lesson)
  <tr>
    <th scope="row">{{$lesson->idLesson}}</th>
    <td>{{$lesson->lesName}}</td>
    <td>{{$lesson->category->catName}}</td>
    <td align="center">
      @if ($lesson->lesArchived)
        <span class="glyphicon glyphicon-ok"></span>
      @else
        <span class="glyphicon glyphicon-remove cross"></span>
      @endif
    </td>
    <td class="actions">
      @if ($lesson->lesArchived)
        <a href="{{route('unarchiveLesson', ['id' => $lesson->idLesson])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="désarchiver"></a>
      @else
        <a href="{{route('archiveLesson', ['id' => $lesson->idLesson])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="archiver"></a>
      @endif
      <a href="{{route('editLesson', ['id' => $lesson->idLesson])}}"><span class="glyphicon glyphicon-edit"></span></a>
    </td>
  </tr>
@endforeach
