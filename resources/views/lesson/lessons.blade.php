@extends('layouts.app')

@section('title')
    Cours
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">Cours</h1>

  @include('includes.session-message')
  @include('includes.search-form', ['route' => 'lessons'])

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col">Catégorie</th>
        <th scope="col" class="text-center">Archivé</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('addLesson')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="lessons-container">
        @include('lesson.data')
    </tbody>
  </table>
@endsection
