@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    Modifier {{ $student->stuFirstname.' '.$student->stuLastname }}
  @elseif ($action == 'add')
    Ajouter un élève
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">
    @if ($action == 'edit')
      <h2>{{ $student->stuFirstname.' '.$student->stuLastname }}</h2>
    @elseif ($action == 'add')
      <h2>Ajouter un élève</h2>
    @endif
  </h1>

  <form method="POST" enctype="multipart/form-data"
  @if ($action == 'edit')
    action="{{ route('editStudent', ['id' => $student->idStudent]) }}"
  @elseif ($action == 'add')
    action="{{ route('addStudent') }}"
  @endif
  >
    {{ csrf_field() }}

    @include('includes.session-message')

    <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Prénom</span>
          </div>
          <input type="text" class="form-control" name="firstname" value="{{isset($student) ? $student->stuFirstname : old('firstname') }}" required>
        </div>


        @if ($errors->has('firstname'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('firstname') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Nom</span>
          </div>
          <input type="text" class="form-control" name="lastname" value="{{isset($student) ? $student->stuLastname : old('lastname') }}" required>
        </div>


        @if ($errors->has('lastname'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('lastname') }}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group {{ $errors->has('class') ? ' has-error' : '' }}">
      <div class="col">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Classe</label>
          </div>
          <select class="bootstrap-select" name="class" data-live-search="true" data-width="1%" required>
            <option disabled selected value> -- Choisissez la classe -- </option>
            @foreach ($classes as $class)
              <option value="{{$class->idClass}}" {{((isset($student) && $student->class == $class ) || old('class') == $class->idClass) ? 'selected' : ''}}>{{$class->claName}}</option>
            @endforeach
          </select>
        </div>

          @if ($errors->has('class'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('class') }}
              </div>
          @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col">
          <button type="submit" class="btn btn-primary">
            @if ($action == 'edit')
              Modifier
            @elseif ($action == 'add')
              Ajouter
            @endif
          </button>
      </div>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
