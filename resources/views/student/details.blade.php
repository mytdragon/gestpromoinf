@extends('layouts.app')

@section('title')
    {{$student->stuFirstname.' '.$student->stuLastname}}
@endsection

@push('css')

@endpush

@section('content')
  <h1 class="mt-4">{{$student->stuFirstname.' '.$student->stuLastname}}</h1>

  @include('includes.session-message')

  <div class="mb-2">
    <button id="add-semester-button" type="button" class="btn btn-light">Ajouter un semestre</button>
    <button id="add-semester-repetition-button" type="button" class="btn btn-light">Ajouter un semestre redoublé</button>
    <button id="show-semesters-button" type="button" class="btn btn-light" value="0"><span>Afficher</span> semestres redoublés</button>
    <a href="{{route('downloadPDFStudent', ['id' => $student->idStudent])}}"><button id="export-followup-pdf" type="button" class="btn btn-light"><span class="glyphicon glyphicon-download-alt"></span> Exporter</button></a>
  </div>

  <small><i>Lorsqu'une note est ajoutée, changée, ou supprimée, il est nécessaire de recharger la page pour visualiser les nouvelles moyennes...</i></small>
  <div class="followup">
    @include('student.followup')
  </div>

  @push('js')
    <script type="text/javascript">
      var student = {{$student->idStudent}};
      var addGradeUrl = '{{route('addGrade')}}';
      var editGradeUrl = '{{route('editGrade')}}';
      var removeGradeUrl = '{{route('removeGrade')}}';
      var setPromotionUrl = '{{route('setPromotionStudent')}}';
      var addRepeatSemesterUrl = '{{route('addRepeatSemester')}}';
      var promotions = {!! $promotions !!};
      var repeatedSemesters = {!! json_encode($gradesFormated['semesters']['repeated']) !!};
      var innactiveSemesters = {!! json_encode($gradesFormated['semesters']['innactives']) !!};
      var maxSemester = {!! $gradesFormated['semesters']['no'] ? '"'.end($gradesFormated['semesters']['no']).'"' : "'0'" !!}.replace('\'','');
      var higherSemester = {{count($gradesFormated['semesters']['countIndex']) > 0 ?  str_replace('\'', '', $gradesFormated['semesters']['no'][end($gradesFormated['semesters']['countIndex'])]) : 0}};
      var years = {{$gradesFormated['years']['count']}};
      var maxYears = {{$years}};
    </script>

    <script src="{{ asset('js/grade.js?v='.config('app.version')) }}"></script>
  @endpush
@endsection
