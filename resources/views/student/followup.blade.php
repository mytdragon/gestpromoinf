<table id="grades-table" class="table table-sm table-bordered table-hover">
  <thead class="thead-light">
    <tr>
      <th scope="col" class="grades-header">Semestres</th>
      @foreach ($gradesFormated['semesters']['no'] as $semester)
        <th scope="col" class="text-center grades-header semester-header">{{$semester}}</th>
      @endforeach
      <th scope="col" class="text-center grades-header">AVG</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($gradesFormated['categories'] as $category => $categoryDetails)
      <tr class="tr-borderless" data-toggle="collapse" data-target=".cat-{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}">
        <th>
          <span class="glyphicon glyphicon-triangle-bottom"></span>
          {{$category}}
        </th>

        @for ($i = 0; $i < count($gradesFormated['semesters']['no']); $i++)
          <th id="cat{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}-sem{{str_replace('\'','',$gradesFormated['semesters']['no'][$i])}}-r{{str_contains($gradesFormated['semesters']['no'][$i], '\'') ? 1 : 0}}" scope="col" class="text-center">{{(isset($categoryDetails['avgSemesters']) && $categoryDetails['avgSemesters'][$i]) ? roundTenth($categoryDetails['avgSemesters'][$i]) : ''}}</th>
        @endfor

        <th id="cat{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}-global"class="text-center">{{isset($categoryDetails['avgGlobal']) && $categoryDetails['avgGlobal'] ? roundTenth($categoryDetails['avgGlobal']) : ''}}</th>
      </tr>

      @foreach ($categoryDetails['lessons'] as $lesson => $lessonDetails)
        <tr class="collapse cat-{{preg_replace('/\W+/','',strtolower(strip_tags($category)))}}">
          <td class="lesson" scope="col">{{$lesson}}</td>
          @foreach ($lessonDetails['grades'] as $grade)
            <td class="grade" align="center">{{$grade}}</td>
          @endforeach
          <td id="lesson{{preg_replace('/\W+/','',strtolower(strip_tags($lesson)))}}-sem-avg" align="center">{{isset($lessonDetails['avgGlobal']) && $lessonDetails['avgGlobal'] ? roundFifth($lessonDetails['avgGlobal']) : ''}}</td>
        </tr>
      @endforeach
    @endforeach

    <!-- Modules -->
    <tr>
      <th>Modules (école et CIE)</th>
      @for ($i = 0; $i < count($gradesFormated['semesters']['no']); $i++)
        <th id="modules-sem{{str_replace('\'','',$gradesFormated['semesters']['no'][$i])}}-r{{str_contains($gradesFormated['semesters']['no'][$i], '\'') ? 1 : 0}}" scope="col" class="text-center">{{$gradesFormated['otherAverages']['semesters']['modules'][$i] ? roundTenth($gradesFormated['otherAverages']['semesters']['modules'][$i]) : ''}}</th>
      @endfor
      <th id="modules-global" scope="col" class="text-center">{{isset($gradesFormated['otherAverages']['avgGlobal']['modules']) ? roundTenth($gradesFormated['otherAverages']['avgGlobal']['modules']) : ''}}</th>
    </tr>

    <!-- Global CFC -->
    <tr>
      <th>Global CFC</th>
      @for ($i = 0; $i < count($gradesFormated['semesters']['no']); $i++)
        <th id="globalcfc-sem{{str_replace('\'','',$gradesFormated['semesters']['no'][$i])}}-r{{str_contains($gradesFormated['semesters']['no'][$i], '\'') ? 1 : 0}}" scope="col" class="text-center">{{$gradesFormated['otherAverages']['semesters']['globalCFC'][$i] ? roundTenth($gradesFormated['otherAverages']['semesters']['globalCFC'][$i]) : ''}}</th>
      @endfor
      <th scope="col" class="text-center"></th>
    </tr>
  </tbody>
</table>

@if ($gradesFormated['semesters']['no'] != [])
  <table id="year-avg-table" class="table table-sm table-bordered table-hover">
    <thead class="thead-light">
      <tr>
        <th scope="col" class="">Années</th>
        @for ($i=1; $i <= $years; $i++)
          <th scope="col" class="year-header">{{$i}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @if (in_array($student->class->formation->forName, ['CIN', 'CID']))
        <!-- CBE -->
        <tr>
          <td class="year-avg-title" scope="col">CBE</td>
          @for ($i=1; $i <= $years; $i++)
            <td id="cbe-year{{$i}}" scope="col" class="">{{isset($gradesFormated['categories']['CBE']['avgYears'][$i]) ? roundTenth($gradesFormated['categories']['CBE']['avgYears'][$i]) : ''}}</td>
          @endfor
        </tr>

        <!-- ECG -->
        <tr>
          <td class="year-avg-title" scope="col">ECG</td>
          @for ($i=1; $i <= $years; $i++)
            <td id="ecg-year{{$i}}" scope="col">{{isset($gradesFormated['categories']['ECG']['avgYears'][$i]) ? roundTenth($gradesFormated['categories']['ECG']['avgYears'][$i]) : ''}}</td>
          @endfor
        </tr>
      @endif

      @if (in_array($student->class->formation->forName, ['MIN', 'MID']))
        <!-- MPT -->
        <tr>
          <td class="year-avg-title" scope="col">MPT</td>
          @for ($i=1; $i <= $years; $i++)
            <td id="mpt-year{{$i}}" scope="col">{{isset($gradesFormated['categories']['Théorie MPT']['avgYears'][$i]) ? roundTenth($gradesFormated['categories']['Théorie MPT']['avgYears'][$i]) : ''}}</td>
          @endfor
        </tr>
      @endif

      <!-- Modules -->
      <tr>
        <td class="year-avg-title" scope="col">Modules (école et CIE)</td>
        @for ($i=1; $i <= $years; $i++)
          <td id="modules-year{{$i}}" scope="col">{{isset($gradesFormated['otherAverages']['years']['modules'][$i]) ? roundTenth($gradesFormated['otherAverages']['years']['modules'][$i]) : ''}}</td>
        @endfor
      </tr>

      <!-- Pratique -->
      <tr>
        <td class="year-avg-title" scope="col">Pratique</td>
        @for ($i=1; $i <= $years; $i++)
          <td id="pratique-year{{$i}}" scope="col">{{isset($gradesFormated['categories']['Pratique']['avgYears'][$i]) ? roundTenth($gradesFormated['categories']['Pratique']['avgYears'][$i]) : ''}}</td>
        @endfor
      </tr>

      <!-- global CFC -->
      <tr>
        <td class="year-avg-title" scope="col">Global CFC</td>
        @for ($i=1; $i <= $years; $i++)
          <td id="globalcfc-year{{$i}}" scope="col">{{isset($gradesFormated['otherAverages']['years']['globalCFC'][$i]) ? roundTenth($gradesFormated['otherAverages']['years']['globalCFC'][$i]) : ''}}</td>
        @endfor
      </tr>
    </tbody>
  </table>
@endif

<table id="promotion-table" class="table table-sm table-bordered table-hover {{$gradesFormated['semesters']['no'] == [] ? 'd-none' : ''}}">
  <thead class="thead-light">
    <tr>
      <th scope="col" colspan="2" class="">Promotions</th>
    </tr>
  </thead>
  <tbody>
    @if ($gradesFormated['semesters']['no'] != [])
      @foreach ($gradesFormated['semesters']['no'] as $no)
        <tr class="promotion-tr">
          <th scope="col" class="promotion-semester">{{$no}}</th>
          <td>
            <select class="form-control promotion-select" id="exampleFormControlSelect1" data-semester="{{$no}}">
              <option selected value="0">---</option>
              @foreach ($promotions as $promotion)
                <option {{$gradesFormated['promotions'][$no]['id'] == $promotion->idPromotion ? 'selected' : ''}} value="{{$promotion->idPromotion}}">{{$promotion->proName}}</option>
              @endforeach
            </select>
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
