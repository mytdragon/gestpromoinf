<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/glyphicons.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-select.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/app.css?v='.config('app.version')) }}" rel="stylesheet">
</head>

<body>
  <h1 class="mt-4">{{$student->stuFirstname.' '.$student->stuLastname}} - {{$student->class->claName}}</h1>

  <table id="grades-table" class="table table-sm table-bordered table-hover">
    <thead class="thead-light">
      <tr>
        <th scope="col" class="grades-header">Semestres</th>
        @foreach ($gradesFormated['semesters']['done'] as $semester)
          <th scope="col" class="text-center grades-header semester-header">{{$semester}}</th>
        @endforeach
        <th scope="col" class="text-center grades-header">AVG</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($gradesFormated['categories'] as $category => $categoryDetails)
        <tr class="tr-borderless">
          <th>
            {{$category}}
          </th>

          @foreach ($gradesFormated['semesters']['doneIndexs'] as $i)
            <th scope="col" class="text-center">{{(isset($categoryDetails['avgSemesters']) && $categoryDetails['avgSemesters'][$i]) ? roundTenth($categoryDetails['avgSemesters'][$i]) : ''}}</th>
          @endforeach

          <th class="text-center">{{isset($categoryDetails['avgGlobal']) && $categoryDetails['avgGlobal'] ? roundTenth($categoryDetails['avgGlobal']) : ''}}</th>
        </tr>

        @foreach ($categoryDetails['lessons'] as $lesson => $lessonDetails)
          <tr>
            <td class="lesson" scope="col">{{$lesson}}</td>
            @foreach ($gradesFormated['semesters']['doneIndexs'] as $i)
              <td class="grade" align="center">{{$lessonDetails['grades'][$i]}}</td>
            @endforeach
            <td align="center">{{isset($lessonDetails['avgGlobal']) && $lessonDetails['avgGlobal'] ? roundFifth($lessonDetails['avgGlobal']) : ''}}</td>
          </tr>
        @endforeach
      @endforeach

      <!-- Modules -->
      <tr>
        <th>Modules (école et CIE)</th>
        @foreach ($gradesFormated['semesters']['doneIndexs'] as $i)
          <th scope="col" class="text-center">{{$gradesFormated['otherAverages']['semesters']['modules'][$i] ? roundTenth($gradesFormated['otherAverages']['semesters']['modules'][$i]) : ''}}</th>
        @endforeach
        <th scope="col" class="text-center">{{$gradesFormated['otherAverages']['avgGlobal']['modules'] ? roundTenth($gradesFormated['otherAverages']['avgGlobal']['modules']) : ''}}</th>
      </tr>

      <!-- Global CFC -->
      <tr>
        <th>Global CFC</th>
        @foreach ($gradesFormated['semesters']['doneIndexs'] as $i)
          <th scope="col" class="text-center">{{$gradesFormated['otherAverages']['semesters']['globalCFC'][$i] ? roundTenth($gradesFormated['otherAverages']['semesters']['globalCFC'][$i]) : ''}}</th>
        @endforeach
        <th scope="col" class="text-center">{{$gradesFormated['otherAverages']['avgGlobal']['globalCFC'] ? roundTenth($gradesFormated['otherAverages']['avgGlobal']['globalCFC']) : ''}}</th>
      </tr>
    </tbody>
  </table>

  @if ($gradesFormated['semesters']['no'] != [])
    <table id="year-avg-table" class="table table-sm table-bordered table-hover">
      <thead class="thead-light">
        <tr>
          <th scope="col" class="">Années</th>
          @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
            <th scope="col" class="">{{$i}}</th>
          @endfor
        </tr>
      </thead>
      <tbody>
        @if (in_array($student->class->formation->forName, ['CIN', 'CID']))
          <!-- CBE -->
          <tr>
            <td class="year-avg-title" scope="col">CBE</td>
            @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
              <td scope="col" class="">{{$gradesFormated['categories']['CBE']['avgYears'][$i] ? roundTenth($gradesFormated['categories']['CBE']['avgYears'][$i]) : ''}}</td>
            @endfor
          </tr>

          <!-- ECG -->
          <tr>
            <td class="year-avg-title" scope="col">ECG</td>
            @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
              <td scope="col" class="">{{$gradesFormated['categories']['ECG']['avgYears'][$i] ? roundTenth($gradesFormated['categories']['ECG']['avgYears'][$i]) : ''}}</td>
            @endfor
          </tr>
        @endif

        @if (in_array($student->class->formation->forName, ['MIN', 'MID']))
          <!-- MPT -->
          <tr>
            <td class="year-avg-title" scope="col">MPT</td>
            @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
              <td scope="col" class="">{{$gradesFormated['categories']['Théorie MPT']['avgYears'][$i] ? roundTenth($gradesFormated['categories']['Théorie MPT']['avgYears'][$i]) : ''}}</td>
            @endfor
          </tr>
        @endif

        <!-- Modules -->
        <tr>
          <td class="year-avg-title" scope="col">Modules (école et CIE)</td>
          @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
            <td scope="col" class="">{{$gradesFormated['otherAverages']['years']['modules'][$i] ? roundTenth($gradesFormated['otherAverages']['years']['modules'][$i]) : ''}}</td>
          @endfor
        </tr>

        <!-- Pratique -->
        <tr>
          <td class="year-avg-title" scope="col">Pratique</td>
          @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
            <td scope="col" class="">{{$gradesFormated['categories']['Pratique']['avgYears'][$i] ? roundTenth($gradesFormated['categories']['Pratique']['avgYears'][$i]) : ''}}</td>
          @endfor
        </tr>

        <!-- global CFC -->
        <tr>
          <td class="year-avg-title" scope="col">Global CFC</td>
          @for ($i=1; $i <= $gradesFormated['years']['count']; $i++)
            <td scope="col" class="">{{$gradesFormated['otherAverages']['years']['globalCFC'][$i] ? roundTenth($gradesFormated['otherAverages']['years']['globalCFC'][$i]) : ''}}</td>
          @endfor
        </tr>
      </tbody>
    </table>
  @endif


  <table id="promotion-table" class="table table-sm table-bordered table-hover {{$gradesFormated['semesters']['no'] == [] ? 'd-none' : ''}}">
    <thead class="thead-light">
      <tr>
        <th scope="col" colspan="2" class="">Promotions</th>
      </tr>
    </thead>
    <tbody>
      @if ($gradesFormated['semesters']['done'] != [])
        @foreach ($gradesFormated['semesters']['done'] as $no)
          <tr class="promotion-tr">
            <th scope="col" class="">{{$no}}</th>
            <td>
              {{$gradesFormated['promotions'][$no]['name']}}
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>

</body>
</html>
