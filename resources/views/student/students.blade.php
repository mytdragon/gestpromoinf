@extends('layouts.app')

@section('title')
    Elèves
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">Elèves</h1>

  @include('includes.session-message')
  @include('includes.search-form', ['route' => 'students'])

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Prénom</th>
        <th scope="col">Nom</th>
        <th scope="col">Classe</th>
        <th scope="col" class="text-center">Archivé</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('addStudent')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="students-container">
        @include('student.data')
    </tbody>
  </table>

  @push('js')

  @endpush
@endsection
