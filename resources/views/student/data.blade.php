@foreach ($students as $student)
  <tr>
    <th scope="row">{{$student->idStudent}}</th>
    <td>{{$student->stuFirstname}}</td>
    <td>{{$student->stuLastname}}</td>
    <td>{{$student->class->claName}}</td>
    <td align="center">
      @if ($student->stuArchived)
        <span class="glyphicon glyphicon-ok"></span>
      @else
        <span class="glyphicon glyphicon-remove cross"></span>
      @endif
    </td>
    <td class="actions">
      @if ($student->stuArchived)
        <a href="{{route('unarchiveStudent', ['id' => $student->idStudent])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="désarchiver"></a>
      @else
        <a href="{{route('archiveStudent', ['id' => $student->idStudent])}}"><img class="archive-icon" src="{{asset('/img/folder.svg')}}" alt="archiver"></a>
      @endif
      <a href="{{route('editStudent', ['id' => $student->idStudent])}}"><span class="glyphicon glyphicon-edit"></span></a>
      <a href="{{ route('viewStudent', ['id' => $student->idStudent]) }}"><span class="glyphicon glyphicon-education"></span></a>
    </td>
  </tr>
@endforeach
