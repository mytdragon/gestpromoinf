@extends('layouts.app')

@section('title')
    Accueil
@endsection

@push('css')

@endpush

@section('content')

  <h1 class="mt-4">Accueil</h1>
  <p>Bienvenue sur le système de promotions OwO ! Cette application a été développée dans le cadre du projet pré-TPI par Loïc Herzig.</p>

  @push('js')

  @endpush
@endsection
