<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Gest Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during gest of various eloquent
    | model that we need to display to the user.
    |
    */

    /* General */
    'addSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement ajouté `:model`.'],
    'archiveSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement archivé `:model`.'],
    'editSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement modifié `:model`.'],
    'importationDone' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement importé le fichier Excel.'],
    'modelWithNameAlreadyExists' => ['title' => 'Echec!', 'msg' => 'Le nom est déjà utilisé par un(e) autre :model.'],
    'unarchiveSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement désarchivé `:model`.'],

    /* Student */
    'cannotChangeFormation' => ['title' => 'Echec!', 'msg' => 'Changement de formation impossible.'],
    'normalSemesterNotDone' => ['title' => 'Echec!', 'msg' => 'Le semestre \':semester\' n\'a pas été fait, il n\'est pas possible de le redoubler.'],
    'noSemester' => ['title' => 'Echec!', 'msg' => 'Impossible d\'exporter une classe qui n\'a pas de note.'],
    'noStudent' => ['title' => 'Echec!', 'msg' => 'Impossible d\'exporter une classe qui n\'a pas d\'élèves.'],

    /* Teacher */
    'teacherWithAcronymAlreadyExists' => ['title' => 'Echec!', 'msg' => 'Un enseignant avec cet acronym existe déjà.'],

];
