<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
Route::get('login','AuthController@showLoginForm')->name('login');
Route::post('login','AuthController@login')->name('login');
Route::get('logout','AuthController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {
  Route::get('/', function () {
      return view('home');
  })->name('home');

  Route::get('/classes', 'ClassController@showClasses')->name('classes');
  Route::get('/students', 'StudentController@showStudents')->name('students');

  // Require admin
  Route::middleware(['checkAdmin'])->group(function () {
    // Class's routes
    Route::get('/class/add', 'ClassController@showAdd')->name('addClass');
    Route::post('/class/add', 'ClassController@add')->name('addClass');
    Route::get('/class/view/{id}/{semester?}', 'ClassController@view')->name('viewClass');
    Route::get('/class/edit/{id}', 'ClassController@showEdit')->name('editClass');
    Route::post('/class/edit/{id}', 'ClassController@edit')->name('editClass');
    Route::get('/class/archive/{id}', 'ClassController@archive')->name('archiveClass');
    Route::get('/class/unarchive/{id}', 'ClassController@unarchive')->name('unarchiveClass');
    Route::get('/class/download/{id}/all', 'ClassController@downloadByAllPDF')->name('downloadByAllPDFClass');
    Route::get('/class/download/{id}/{semester}', 'ClassController@downloadBySemesterPDF')->name('downloadBySemesterPDFClass');

    // Student's routes
    Route::get('/student/add', 'StudentController@showAdd')->name('addStudent');
    Route::post('/student/add', 'StudentController@add')->name('addStudent');
    Route::get('/student/view/{id}', 'StudentController@view')->name('viewStudent');
    Route::get('/student/edit/{id}', 'StudentController@showEdit')->name('editStudent');
    Route::post('/student/edit/{id}', 'StudentController@edit')->name('editStudent');
    Route::get('/student/archive/{id}', 'StudentController@archive')->name('archiveStudent');
    Route::get('/student/unarchive/{id}', 'StudentController@unarchive')->name('unarchiveStudent');
    Route::post('/student/promotion', 'StudentController@setPromotion')->name('setPromotionStudent');
    Route::get('/student/download/{id}', 'StudentController@downloadPDF')->name('downloadPDFStudent');

    // Teacher's routes
    Route::get('/teachers', 'TeacherController@showTeachers')->name('teachers');
    Route::get('/teacher/add', 'TeacherController@showAdd')->name('addTeacher');
    Route::post('/teacher/add', 'TeacherController@add')->name('addTeacher');
    Route::get('/teacher/edit/{id}', 'TeacherController@showEdit')->name('editTeacher');
    Route::post('/teacher/edit/{id}', 'TeacherController@edit')->name('editTeacher');
    Route::get('/teacher/archive/{id}', 'TeacherController@archive')->name('archiveTeacher');
    Route::get('/teacher/unarchive/{id}', 'TeacherController@unarchive')->name('unarchiveTeacher');

    // Lesson's routes
    Route::get('/lessons', 'LessonController@showLessons')->name('lessons');
    Route::get('/lesson/add', 'LessonController@showAdd')->name('addLesson');
    Route::post('/lesson/add', 'LessonController@add')->name('addLesson');
    Route::get('/lesson/edit/{id}', 'LessonController@showEdit')->name('editLesson');
    Route::post('/lesson/edit/{id}', 'LessonController@edit')->name('editLesson');
    Route::get('/lesson/archive/{id}', 'LessonController@archive')->name('archiveLesson');
    Route::get('/lesson/unarchive/{id}', 'LessonController@unarchive')->name('unarchiveLesson');

    // Grade's routes
    Route::post('/grade/add', 'GradeController@add')->name('addGrade');
    Route::post('/grade/edit', 'GradeController@edit')->name('editGrade');
    Route::post('/grade/remove', 'GradeController@remove')->name('removeGrade');
    Route::post('/grade/add_repeat_semester', 'GradeController@addRepeatSemester')->name('addRepeatSemester');

    // Import
    Route::post('/import', 'ImportController@import')->name('import');
    Route::post('/conflict/grade/replace', 'ImportController@replaceGrade')->name('replaceGrade');
    Route::post('/conflict/grade/repeat', 'ImportController@repeatGrade')->name('repeatGrade');
  });
});

// Test route
Route::get('/devtestroute', 'TestController@test')->name('devtest');
