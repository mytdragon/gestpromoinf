/**
 * ETML
 * Author: Loïc Herzig
 * Date: 14.03.2019
 * Description: Javascript for import file related
 */

// Write file name on input file
$('input[type="file"]').change(function(e){
  var fileName = e.target.files[0].name;
  $('.custom-file-label').html(fileName);
});

$('body').on('submit', "#import-excel-file", function (event) {
  // Get semester that we cannot get with excel file
  var semester = prompt("Semestre");
  if (semester == null|| semester === ""){ return false; }
  $('input[name=semester]').val(semester);
});

$('body').on('click', ".grade-keep", function (event) {
  var student = $(this).parents('tr').data('student');

  $(this).parents('tr').remove();

  // Remove student's tr if no more single conflict
  if ($('.student-' + student).length == 0){
    $('#group-student-' + student).remove();
  }

  redirectIfNoContent();

  return false;
});

$('body').on('click', '.grade-keep-all', function (event) {
  var tr = $(this).parents('tr');
  var student = tr.data('student');

  // Remove conflicts on view
  tr.remove();
  $('.student-' + student).remove();

  redirectIfNoContent();

  return false;
});

$('body').on('click', ".grade-replace", function (event) {
  var url = $(this).attr('href');
  var tr = $(this).parents('tr');
  var student = tr.data('student');
  var changes = [{
    'oldGrade' : tr.data('grade'),
    'newGrade' : tr.data('new'),
  }];

  $.ajax({
    type: 'POST',
    url: url,
    data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     changes: changes,
    },
    success:function(data){
     if (data.success){
       // Remove the conflict on view
       $(tr).remove();

       // Remove student's tr if no more single conflict
       if ($('.student-' + student).length == 0){
         $('#group-student-' + student).remove();
       }

       redirectIfNoContent();
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
    }
  });

  return false;
});

$('body').on('click', ".grade-replace-all", function (event) {
  var tr = $(this).parents('tr');
  var student = tr.data('student');
  var url = $(this).attr('href');
  var changes = [];

  $('.student-' + student).each(function () {
    changes.push({
      'oldGrade' : $(this).data('grade'),
      'newGrade' : $(this).data('new'),
    });
  });

  $.ajax({
    type: 'POST',
    url: url,
    data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     changes: changes,
    },
    success:function(data){
     if (data.success){
       // Remove the conflict on view
       $(tr).remove();
       $('.student-' + student).remove();

       redirectIfNoContent();
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
    }
  });

  return false;
});

$('body').on('click', ".grade-repeat", function (event) {
  var url = $(this).attr('href');
  var tr = $(this).parents('tr');
  var student = tr.data('student');
  var changes = [{
    'oldGrade' : tr.data('grade'),
    'newGrade' : tr.data('new'),
  }];

  $.ajax({
    type: 'POST',
    url: url,
    data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     changes: changes,
    },
    success:function(data){
     if (data.success){
       // Remove the conflict on view
       $(tr).remove();

       // Remove student's tr if no more single conflict
       if ($('.student-' + student).length == 0){
         $('#group-student-' + student).remove();
       }

       redirectIfNoContent();
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
    }
  });

  return false;
});

$('body').on('click', '.grade-repeat-all', function (event) {
  var tr = $(this).parents('tr');
  var student = tr.data('student');
  var url = $(this).attr('href');
  var changes = [];

  $('.student-' + student).each(function () {
    changes.push({
      'oldGrade' : $(this).data('grade'),
      'newGrade' : $(this).data('new'),
    });
  });

  $.ajax({
    type: 'POST',
    url: url,
    data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     changes: changes,
    },
    success:function(data){
     if (data.success){
       // Remove the conflict on view
       $(tr).remove();
       $('.student-' + student).remove();
       redirectIfNoContent();
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
    }
  });

  return false;
});

$('body').on('submit', '#personal-informations-teacher-form', function (event) {
  event.preventDefault();

  $.ajax({
    type: $(this).attr('method'),
    url: $(this).attr('action'),
    data: $(this).serialize(),
    success: function (data) {
      console.log(data.msg);
      $('#teacher').remove();
      redirectIfNoContent();
    }
  });
});

function redirectIfNoContent(){
  if ($('#teacher').length == 0 && $('#conflicts tbody').children().length == 0){
    window.location.href = $('input[name="redirect"]').val();
  }
}
