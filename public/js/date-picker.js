/**
 * Add a jqueryUI datepicker on input date for browser that does not support date.
 *
 * Execute as soon as the page's Document Object Model (DOM) becomes safe to manipulate.
 */
$( document ).ready(function() {
  if ( $('[type="date"]').prop('type') != 'date' ) {
      $('[type="date"]').datepicker({ dateFormat: 'yy-mm-dd' });
  }
});
