/**
 * ETML
 * Author: Loïc Herzig
 * Date: 15.02.2019
 * Description: Scripts for student's grades related on student follow-up
 * Require:
 *    student
 *    addGradeUrl
 *    editGradeUrl
 *    removeGradeUrl
 *    setPromotionUrl
 *    addRepeatSemesterUrl
 *    promotions
 *    repeatedSemesters
 *    innactiveSemesters
 *    maxSemester
 *    higherSemester
 *    years
 *    maxYears
 */

$( document ).ready(function() {
  $('body').on('click', "#add-semester-button", function (event) {
    // Return if last of repeatedSemesters is in innactiveSemesters
    var lastRepeat = repeatedSemesters.slice(-1).pop();
    var innactive = innactiveSemesters.find(function(element) {
      return element == lastRepeat + '\'';
    });

    if (innactive != undefined){ return; }

    if (higherSemester == maxSemester) { return; };
    higherSemester++;

    // Show semester grades column
    $('.semester-header:contains("'+ higherSemester + '")')
    .filter(function() {
      return $(this).text() === String(higherSemester);
    })
    .each(function () {
        var index = $(this).index();

        // Add class "hideable" for the original semester and hide it
        $('#grades-table').find('tr')
        .each(function(){
          $(this).find('td').eq(index).show();
          $(this).find('th').eq(index).show();
        });
    });

    // Show semester promotion row
    $('th.promotion-semester:contains("' + higherSemester + '")')
    .filter(function() {
      return $(this).text() === String(higherSemester);
    })
    .each(function() {
      $(this).parents('tr.promotion-tr').show();
    });

    if (higherSemester % 2 == 0){
      years = higherSemester / 2;
      $('.year-header:contains("' + years + '")')
      .filter(function() {
        return $(this).text() === String(years);
      })
      .each(function() {
        var index = $(this).index();

        // Add class "hideable" for the original semester and hide it
        $('#year-avg-table').find('tr').each(function(){
          $(this).find('td').eq(index).show();
          $(this).find('th').eq(index).show();
        });
      });
    }
  });

  $('body').on('click', "#add-semester-repetition-button", function (event) {
    var semester;
    if (repeatedSemesters.length == 2){
      var lastRepeat = repeatedSemesters.slice(-1).pop();
      var innactive = innactiveSemesters.find(function(element) {
        return element == lastRepeat + '\'';
      });

      if (innactive == undefined){ return; }

      semester = lastRepeat;

      // Increment higherSemester if lower than the repeated one
      if (higherSemester < semester){ higherSemester = semester; }
    }
    else if (repeatedSemesters.length == 1){
      var lastRepeat = repeatedSemesters.slice(-1).pop();
      var semester = parseInt(lastRepeat) + 1
      var innactive = innactiveSemesters.find(function(element) {
        return element == semester + '\'';
      });

      if (innactive == undefined){ return; }

      repeatedSemesters.push(semester);
      higherSemester = semester;

      // Increment higherSemester if lower than the repeated one
      if (higherSemester < semester){ higherSemester = semester; }
    }
    else {
      if (higherSemester == 0){ return; }
      semester = prompt("Semestre redoublé");

      if (semester == null || semester == '' || semester == 0 || semester > higherSemester){ return; }

      if (repeatedSemesters.length == 0) {
        repeatedSemesters.push(semester);
        repeatedSemesters.push(parseInt(semester) + 1);
        higherSemester = semester;
      }
    }

    $.ajax({
     type: 'POST',
     url: addRepeatSemesterUrl,
     data: {
       _token : $('meta[name="csrf-token"]').attr('content'),
       student : student,
       semester : semester,
     },
     success:function(data){
       if (data.success){
         var semFormated = semester + "'";
         // grades column
         $('.semester-header:contains("'+ semFormated + '")')
         .filter(function() {
           return $(this).text() === String(semFormated);
         })
         .each(function() {
           var index = $(this).index();

           // Add class "hideable" for the original semester and hide it
           $('#grades-table').find('tr').each(function(){
             $(this).find('td').eq(index).show();
             $(this).find('th').eq(index).show();
           });
         });

         // promotion row
         $('th.promotion-semester:contains("' + semFormated + '")')
         .filter(function() {
           return $(this).text() === String(semFormated);
         })
         .each(function() {
           $(this).parents('tr.promotion-tr').show();
         });

         // Remove semester from innactive
         innactiveSemesters.splice( innactiveSemesters.indexOf(semester + '\''), 1 );

         $('div.followup').empty();
         $('div.followup').append(data.html);
         display();
       }
       else {
          var message = "";
          $.each(data.errors, function(key, value){
            message += value + '\n';
          });
          alert(message);
       }
     }
   });
  });

  $('body').on('click', ".grade", function (event) {
    // stops the bubbling of an event to parent elements, preventing any parent event handlers from being executed
    event.stopPropagation();

    var tdGrade = $(event.target);
    var value = $(event.target).html();

    // Cancel if double click on input
    if ($(event.target).is('.dblclick-textbox')) { return; }

    // Detect if value will be added or edited
    if ($.trim(value) === "") {
        $(tdGrade).data('mode', 'add');
    } else {
        $(tdGrade).data('mode', 'edit');
    }

    updateVal(tdGrade, value);
  });

  $('body').on('click', "#show-semesters-button", function (event) {
    $('.semester-hideable').toggle();
    var val = $('#show-semesters-button').val();

    if (val == 0){
      $('#show-semesters-button').val(1);
      $('#show-semesters-button').children('span').text('Cacher');
    }
    else {
      $('#show-semesters-button').val(0);
      $('#show-semesters-button').children('span').text('Afficher');
    }

  });

  $('body').on('change', '.promotion-select', function (event){
    var val = $(this).val();
    var tdText = $(this).parents('tr.promotion-tr').children('th').text();
    var repetition = tdText.includes('\'')
    var semester = tdText.replace('\'','');

    $.ajax({
     type: 'POST',
     url: setPromotionUrl,
     data: {
       _token : $('meta[name="csrf-token"]').attr('content'),
       student : student,
       promotion: val,
       semester: semester,
       repetition: repetition,
     },
     success:function(data){
       if (data.success){
         // Nothing to do..
       }
       else {
          var message = "";
          $.each(data.errors, function(key, value){
            message += value + '\n';
          });
          alert(message);
       }
     }
   });
  });

  display();
});

/**
 * Show only years and semester that count, set hideable repeated semesters and hide innactive semesters
 * @return {null}
 */
function display(){
  // Hide innactive semesters
  innactiveSemesters.forEach(function (element){
    // Hide semester grades column
    $('.semester-header:contains("'+ element + '")')
    .filter(function() {
      return $(this).text() === String(element);
    })
    .each(function () {
        var index = $(this).index();

        // Add class "hideable" for the original semester and hide it
        $('#grades-table').find('tr')
        .each(function(){
          $(this).find('td').eq(index).hide();
          $(this).find('th').eq(index).hide();
        });
    });

    // Hide semester promotion row
    $('th.promotion-semester:contains("' + element + '")')
    .filter(function() {
      return $(this).text() === String(element);
    })
    .each(function() {
      $(this).parents('tr.promotion-tr').hide();
    });
  });

  // Set hideable repeated senesters
  repeatedSemesters.forEach(function (element){
    // grades column
    $('.semester-header:contains("'+ element + '")')
    .filter(function() {
      return $(this).text() === String(element);
    })
    .each(function() {
      var index = $(this).index();

      // Add class "hideable" for the original semester and hide it
      $('#grades-table').find('tr').each(function(){
        $(this).find('td').eq(index).hide().addClass('semester-hideable');
        $(this).find('th').eq(index).hide().addClass('semester-hideable');
      });
    });

    // promotion row
    $('th.promotion-semester:contains("' + element + '")')
    .filter(function() {
      return $(this).text() === String(element);
    })
    .each(function() {
      $(this).parents('tr.promotion-tr').hide().addClass('semester-hideable');
    });
  });

  // Hide year that doesn't count
  for (var i = maxYears; i > years; i--){
    $('.year-header:contains("' + i + '")')
    .filter(function() {
      return $(this).text() === String(i);
    })
    .each(function() {
      var index = $(this).index();

      // Add class "hideable" for the original semester and hide it
      $('#year-avg-table').find('tr').each(function(){
        $(this).find('td').eq(index).hide();
        $(this).find('th').eq(index).hide();
      });
    });
  }
}

/**
 * Update value of td with a textbox
 * @param  {Jquery.object} tdGrade td to show textbox and edit value
 * @param  {float} value   old value
 * @return {null}
 */
function updateVal(tdGrade, value) {
  $(tdGrade).html('<input class="w-100 dblclick-textbox" type="number" step="0.5" min="1" max="6" value="' + value + '" />');

  var mode = $(tdGrade).data('mode');

  $(".dblclick-textbox").focus();
  $(".dblclick-textbox").keyup(function (event) {
    if (event.keyCode == 13) { // Leave textbox and save if press enter
      // Get all grade's informations
      var newGrade = $(this).val();
      var thSemester = tdGrade.closest('table').find('th').eq(tdGrade.index());
      var lesson = tdGrade.parent().children('td.lesson').html();
      var semester, repetition;
      if (thSemester.html().includes('\'')){
        semester = thSemester.html().replace('\'','');
        repetition = 1;
      }
      else {
        semester = thSemester.html();
        repetition = 0;
      }

      // Check the grade
      if (newGrade != '' ){
        if (newGrade < 1 || newGrade > 6) return;
        if (newGrade*10 % 1 != 0) return; // NOTE cannot do % 0.1 because IEEE
      }

      if (mode == 'add'){
        // add if value isn't null
        if (newGrade != '') {
          addGrade($(this), addGradeUrl, student, lesson, semester, repetition, newGrade);
        }
      }
      else if (mode == 'edit') {
        // Remove grade if value is null, else edit grade
        if (newGrade == '') {
          removeGrade($(this), removeGradeUrl, student, lesson, semester, repetition, newGrade);
        }
        else {
          // Edit grade
          editGrade($(this), editGradeUrl, student, lesson, semester, repetition, newGrade);
        }
      }
    }
    else if (event.keyCode == 27){ // Leave textbox without save if press esc
      // Reset the value
      $(this).parent().html(value);
    }
  });
}

/**
 * Send ajax request for add grade
 * @param {Jquery.object} textbox
 * @param {string} addGradeUrl url for add grade
 * @param {int} student     student's id
 * @param {string} lesson      lesson's name
 * @param {int} semester
 * @param {boolean} repetition
 * @param {float} grade       grade to add
 */
function addGrade(textbox, addGradeUrl, student, lesson, semester, repetition, grade){
  $.ajax({
   type: 'POST',
   url: addGradeUrl,
   data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     student : student,
     lesson: lesson,
     semester: semester,
     repetition: repetition,
     grade: grade
   },
   success:function(data){
     if (data.success){
       textbox.parent().html(grade.trim());

       data.avgToChange.forEach(function(element){
         var key = Object.keys(element);
         $('#' + key).text(element[key]);
       });
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
   }
 });
}

/**
 * Send ajax request for add grade
 * @param {Jquery.object} textbox
 * @param {string} addGradeUrl url for add grade
 * @param {int} student     student's id
 * @param {string} lesson      lesson's name
 * @param {int} semester
 * @param {boolean} repetition
 * @param {float} grade       value to edit
 */
function editGrade(textbox, editGradeUrl, student, lesson, semester, repetition, grade){
  $.ajax({
   type: 'POST',
   url: editGradeUrl,
   data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     student : student,
     lesson: lesson,
     semester: semester,
     repetition: repetition,
     grade: grade
   },
   success:function(data){
     if (data.success){
       textbox.parent().html(grade.trim());

       data.avgToChange.forEach(function(element){
         var key = Object.keys(element);
         $('#' + key).text(element[key]);
       });
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
   }
 });
}

/**
 * Send ajax request for add grade
 * @param {Jquery.object} textbox
 * @param {string} addGradeUrl url for add grade
 * @param {int} student     student's id
 * @param {string} lesson      lesson's name
 * @param {int} semester
 * @param {boolean} repetition
 * @param {float} grade       ref grade to remove
 */
function removeGrade(textbox, removeGradeUrl, student, lesson, semester, repetition, grade){
  $.ajax({
   type: 'POST',
   url: removeGradeUrl,
   data: {
     _token : $('meta[name="csrf-token"]').attr('content'),
     student : student,
     lesson: lesson,
     semester: semester,
     repetition: repetition,
     grade: grade
   },
   success:function(data){
     if (data.success){
       textbox.parent().html(grade.trim());

       data.avgToChange.forEach(function(element){
         var key = Object.keys(element);
         $('#' + key).text(element[key]);
       });
     }
     else {
        var message = "";
        $.each(data.errors, function(key, value){
          message += value + '\n';
        });
        alert(message);
     }
   }
 });
}
