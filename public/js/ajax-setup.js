/**
 * ETML
 * Author: Loïc Herzig
 * Date: 07.02.2019
 * Description: Handle ajax errors with statusCode
 */

$( document ).ready(function() {
  $.ajaxSetup({
    statusCode: {
     403: function() {
       alert('403: Vous n\'êtes pas autorisé à effectuer cette action.');
     },
     404: function() {
       alert('404: Une ressource est introuvable.');
     },
     422: function() {
       alert('422: Un champs est manquant ou ne respecte pas le format.');
     },
     500: function() {
       alert('500: Une erreur est survenue.');
     },
    }
  });
});
